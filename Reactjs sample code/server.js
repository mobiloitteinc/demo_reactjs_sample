const express = require('express');
const path =require('path');
const app = express();

app.use(express.static(path.join(__dirname,'build')));

console.log(__dirname);

app.get('/*',(req,res)=>{
    res.sendFile(__dirname +"/build/index.html");
})
let port = process.env.NODE_ENV || 4002;

app.listen(port,()=>console.log(`server is running on port ${port}`));