import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// import firebase from "firebase";

let config = {
  apiKey: "AIzaSyBKAGjaD596bustG_3aMloc1QGZioXHSHw",
  authDomain: "localnano-1525755055589.firebaseapp.com",
  databaseURL: "https://localnano-1525755055589.firebaseio.com",
  projectId: "localnano-1525755055589",
  storageBucket: "localnano-1525755055589.appspot.com",
  messagingSenderId: "850848533079"
};
// firebase.initializeApp(config);

ReactDOM.render(
      <App />,
    document.getElementById('root')
);

// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();
