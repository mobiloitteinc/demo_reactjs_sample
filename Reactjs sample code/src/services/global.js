import { toast } from 'react-toastify';
import { disconnectSocket } from '../services/socket';
import * as $ from 'jquery';

let  BuySellContent = 'BUY';
let browser_fingerprint = null;
let system_info = null;
let  user_detail = null;

// Set token and other details when user login
function login(obj){
    localStorage.setItem('token', obj.data.userId);
    user_detail = obj.data;
    user_token = localStorage.getItem('token');    
    localStorage.setItem('rememberMe',JSON.stringify({remember:obj.remember, email:obj.email}));
}

// se user detail globally
function set_user_detail(data){
    user_token = localStorage.getItem('token'); 
    user_detail = data;
}

// Logout function
function logout(){
    user_detail = null;
    localStorage.removeItem('token');
    user_token = null;
    BuySellContent = 'BUY';
    edit_profile_tab = 'basic_tab';
    security_tab = 'authenticate_tab';
}

// Toast display
function toastDisplay(type, message){
    toast[type](message, {
        position: toast.POSITION.TOP_RIGHT,
        hideProgressBar: true
    });
}

// comma separated function
function numberWithCommas(x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Check browser session
function checkSession(){
    if(localStorage.getItem('token') != null && localStorage.getItem('session') != null) {
        let minDiff = diff_minutes(parseInt(localStorage.getItem('session')), new Date().getTime());
        localStorage.setItem('session',new Date().getTime())
        if(minDiff >= 720){
            localStorage.removeItem('token');
            disconnectSocket();
            window.location.href = window.location.origin;
        } 
    } else localStorage.setItem('session',new Date().getTime())
}

// Minutes difference
function diff_minutes(dt2, dt1) {
    var diff =(dt2 - dt1) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
}

// Decimal calculation
function decimalCalc(value) {
    var with2Decimals = value.toString().match(/^-?\d+(?:\.\d{0,5})?/)[0]
    return with2Decimals;
}

// Removing duplicates from array of objects
function removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

export { BuySellContent, setBuySellContent, user_detail, login, logout, user_token, edit_profile_tab, set_edit_profile_tab, security_tab, set_security_tab, set_user_detail, browser_fingerprint, system_info, set_browser_fingerprint, set_system_info, toastDisplay, loadingSpinner, showLoading, Base64, setHomeFilter, home_filter, changeTabTitle, checkSession, numberWithCommas, decimalCalc, removeDuplicates };
  