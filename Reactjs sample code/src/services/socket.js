import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

import { Base64 } from './global';

let jsWebSocket = null;

// Socket initialization function
function initSocket(user_id, trade_id) {
    jsWebSocket = new WebSocket("ws://12.36.4.113:8181/localNano/"+Base64.encode(user_id)+"/user/"+trade_id);

    jsWebSocket.addEventListener('open', function (event) {
        console.log('js websocket connected');
    });
    
    jsWebSocket.addEventListener('close', function (event) {
        console.log('js websocket disconnected');
    });
}

// Disconnect socket function
function disconnectSocket(){
    if(jsWebSocket == null) return;
    jsWebSocket.close();
}

// Send event
function sendMessage(chat_req){
    jsWebSocket.send(JSON.stringify(chat_req));
}

export { initSocket, sendMessage, disconnectSocket, jsWebSocket };