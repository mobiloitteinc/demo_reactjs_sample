import { browser_fingerprint, user_detail } from './global';
const server_url = 'http://12.36.4.113:8181/';

// API to hit get and post function
function NodeAPI(variables, apiName, apiMethod, token) {
    const baseUrl = server_url + "localNano/" + browser_fingerprint + "/en/";

    var init = apiMethod == "GET" ? {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "userId": (localStorage.getItem('token') == null || apiName == 'fetchPrices') ? '' : localStorage.getItem('token')
        },
    } :
        {
            method: apiMethod,
            headers: {
                'Content-Type': "application/json",
                "userId": (localStorage.getItem('token') == null || apiName == 'fetchPrices') ? '' : localStorage.getItem('token')
            },
            body: JSON.stringify(variables)
        }

    return fetch(baseUrl + apiName, init)
        .then(response => response.json()
            .then(responseData => {
                return responseData;
            }))
        .catch(err => {
            return { responseMessage: "Something went wrong. Please try again." }
        });
}

export { NodeAPI, thirdPartyAPI, doc_url, server_url, machine_detail_api };