// Modules 
import { initialize, localeReducer } from 'react-localize-redux';
import { createStore, combineReducers } from 'redux';
import { addTranslation } from 'react-localize-redux';

// create store from LoacleReducer
export const store = createStore(combineReducers({
    locale: localeReducer
  }));

  // initilize and select default language by language code
const languages = ['en', 'ru'];
store.dispatch(initialize(languages, { defaultLanguage: 'en' }));

// add and dispatch language json file
const translations = require('../assets/json_files/en.json');
store.dispatch(addTranslation(translations));