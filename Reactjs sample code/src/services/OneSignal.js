var OneSignal = window.OneSignal || [];

function initializeOneSignalPush() {
    OneSignal.push(["init", {
        appId: "290b2aa",
        autoRegister: false,
        notifyButton: {
            enable: false /* Set to false to hide */
        }
    }])
    console.log('OneSignal Initialized');
    OneSignal.push(function () {
        console.log('Register For Push');
        OneSignal.push(["registerForPushNotifications"])
    });
    OneSignal.push(function () {
        // Occurs when the user's subscription changes to a new value.
        OneSignal.on('subscriptionChange', function (isSubscribed) {
            console.log("The user's subscription state is now:", isSubscribed);
            OneSignal.getUserId().then(function (userId) {
            console.log("User ID is", userId);
            });
        });
        OneSignal.getUserId().then(function (userId) {
            console.log("User ID is", userId);
        });
        OneSignal.on('notificationDisplay', function (event) {
            console.log("notificationDisplay -> "+JSON.stringify(event));
        });
        OneSignal.on('notificationDismiss', function (event) {
            console.log("notificationDismiss -> "+JSON.stringify(event));
        });
        OneSignal.isPushNotificationsEnabled().then(function(isEnabled) {
            if (isEnabled)
                console.log("Push notifications are enabled!");
            else
                console.log("Push notifications are not enabled yet.");      
        });
    });
}

function oneSignalLogout(){
    OneSignal.push(["setSubscription", false]);
}

export { initializeOneSignalPush, oneSignalLogout };