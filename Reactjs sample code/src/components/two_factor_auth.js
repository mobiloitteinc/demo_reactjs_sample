import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import { BeatLoader } from 'react-spinners';

// Components
import Header from './header';
import Footer from './footer';
import Loadingbar from './loader';

// Services
import { NodeAPI } from '../services/webServices';
import { user_detail, toastDisplay, Base64 } from '../services/global';

class TwoFactorAuth extends Component {

    constructor(props){
        super(props);
        this.state = {
            wholePageLoading: true,
            userDetail: user_detail,
            auth_code: '',
            api_Err_Msg: '',
            auth_check: false,
            auth_check_err: '',
            secretKey: '',
        }
        this.activateTwoFactor = this.activateTwoFactor.bind(this)
    }

    componentDidMount(){
        if(!$("footer.footer").hasClass("navbar-fixed-bottom")) $("footer.footer").addClass("navbar-fixed-bottom");
        this.account_security_api();
    }

    // Get security status and login history list
    account_security_api() {
        Promise.all([NodeAPI({}, 'authenticationStatus?id=' + Base64.encode(this.state.userDetail.userId), 'GET')]).then((data) => {
            if (data[0].responseCode === 200) {
                let security_status = data[0];
                if(security_status.securityStatus.two_factor_auth) {
                    this.props.history.push('/security');
                    return;
                }
                this.setAppContent();
            } else {
                toastDisplay('error', data[0].responseMessage);
            }
        }) .catch(() => { this.setState({wholePageLoading: false}); });
    }

    // Getting the google 2FA details 
    setAppContent() {
        return NodeAPI({}, '2f-authentication?type=22&id=' + Base64.encode(this.state.userDetail.userId), 'GET')
        .then(responseJson => {
            if (responseJson.responseCode === 200) {
                this.setState({secretKey: responseJson.secretKey, wholePageLoading: false});
                let image = JSON.stringify(responseJson.qrCode).split('"');
                $("#googleQr").attr('src', 'data:image/png;base64,' + image[1]);
                setTimeout(() => {$("footer.footer").removeClass("navbar-fixed-bottom");},200)
            } else if(responseJson.responseCode === 600) {
                this.props.history.push('/authorize_device');
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        }).catch(() => { console.log('error api'); });
    }

    handleInputText(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        if (name === "auth_code") {
            this.setState({ [name]: value, api_Err_Msg: '' })
        }
    }

    // Activate 2FA handler
    activateTwoFactor(e) {
        e.preventDefault();
        if(!this.state.auth_check){
            this.setState({auth_check_err: 'Need to be selected'});
            return;
        }
        let auth_request = {
            "userId": this.state.userDetail.userId,
            "code": this.state.auth_code,
            "type":"code",
            "clientTime": new Date().getTime(),
            "afterLogin": false   
        }
        return NodeAPI(auth_request, 'verifySecretKey', 'POST')
            .then(responseJson => {
                if (responseJson.responseCode === 200) {
                    this.props.history.push('/security');
                } else if(responseJson.responseCode === 201) {
                    this.setState({api_Err_Msg: responseJson.responseMessage})
                } else if(responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    // Auth checkbox handler
    handleAuthCheck(e) {
        if(e.target.value)
            this.setState({auth_check_err: ''});
        this.setState({ [e.target.name]: e.target.value == 'true' ? false : true });
    }

    render(){
        return(
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>Enable Authenticator</h1>
                    </div>
                </div>
                {this.state.wholePageLoading ? <center className="loader-other"><BeatLoader /></center> :
                    <div className="narrow">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6 auth-container">
                                <div className="no-border simple-inner-container">
                                    <h2>Activate your new code</h2>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <img className="qr-image" id="googleQr" src={require('../assets/images/qrcode.png')} />
                                        </div>
                                        <div className="col-md-8">
                                            <p>
                                                <b>1.</b> Download the Google Authenticator app for your mobile phone or tablet: <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank">Android</a>, <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">iPhone</a>, <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">iPad and iPod</a>
                                            </p>
                                            <p>
                                                <b>2.</b> Scan the QR-code in the <b> Google Authenticator App</b>
                                            </p>
                                        </div>
                                    </div>
                                    <form className="enable-authentificator btn-dist">
                                        <div className="col-md-12">
                                            <label className="input-mtop-20">Authentication code</label>
                                            <input type="text" placeholder="Enter the 6-digit code shown in your device" name="auth_code" onChange={(event) => this.handleInputText(event)} />
                                            {this.state.api_Err_Msg != '' ? <p className="input-error-text">{this.state.api_Err_Msg}</p> : ''}
                                            <p><b className="text-red">Important!</b></p> 
                                            <p><b className="text-upper-case">{this.state.secretKey}</b></p> 
                                            <p>Write this code on a paper and store it in a safe place. This is back-up code in case you lose your phone. </p>
                                            <div className="custom-checkbox written-code-checkbox">
                                                <input type="checkbox" id="r8" name="auth_check" value={this.state.auth_check} checked={this.state.auth_check} onChange={(event) => { this.handleAuthCheck(event); }} />
                                                <label htmlFor="r8"><span></span>I've written down the code: <b className="text-upper-case">{this.state.secretKey}</b></label>
                                            </div>
                                            {this.state.auth_check_err != '' ? <p className="input-error-text">{this.state.auth_check_err}</p> : ''}
                                        </div>
                                        <input type="submit"  className="button-big lonely-button create-ad-mobile-margin" value="Activate" onClick={this.activateTwoFactor} />
                                        <input type="submit" className="button-big lonely-button orange-bg create-ad-mobile-margin" value="Back" onClick={() => {this.props.history.push('/security')}} />
                                    </form>
                                </div>  
                            </div>
                        </div>
                    </div>
                }

                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default TwoFactorAuth;