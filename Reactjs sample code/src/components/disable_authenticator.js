// Modules
import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';

// Components
import Header from './header';
import Footer from './footer';

// Services
import { user_detail, set_edit_profile_tab, login, set_user_detail, toastDisplay, Base64 } from '../services/global';   
import { validatePhoneNo } from "../services/validation_func";
import { NodeAPI, thirdPartyAPI } from '../services/webServices';

class DisableAuthenticator extends Component {
    constructor(props) { // State initialization
        super(props)
        this.state = {
            userDetail: user_detail,
            mobileVerifyStatus: user_detail != null ? user_detail.isPhoneVerified : false,
            auth_code: '',
            api_Err_Msg: '',
            two_factor_auth_type: '',
            display_sms_label: false,
            heading_msg: ''
        }
        this.verifyCode = this.verifyCode.bind(this);
        this.sendSMS = this.sendSMS.bind(this);
    }

    componentDidMount(){
        this.account_security_api();
    }

    // API to send sms
    sendSMS() {
        return NodeAPI({}, 'sendSms?id='+Base64.encode(this.state.userDetail.userId), 'GET')
        .then(responseJson => {
            if (responseJson.responseCode === 200) {
                this.setState({display_sms_label: !this.state.display_sms_label, api_Err_Msg: '', auth_code: ''});
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    // Get user authentication detail and display heading accordingly
    account_security_api() {
        Promise.all([NodeAPI({}, 'authenticationStatus?id=' + Base64.encode(this.state.userDetail.userId), 'GET')]).then((data) => {
            if(data[0].responseCode === 200) {
                let security_status = data[0];
                this.setState({two_factor_auth_type: security_status.securityStatus.two_factor_auth_type});
                if(security_status.securityStatus.two_factor_auth_type == 11){
                    this.setState({heading_msg: 'Verification code from the printed codes page'});
                }else if(security_status.securityStatus.two_factor_auth_type == 22){
                    this.setState({heading_msg: 'Enter your verification code from your Google Authenticator App'}); // Verification code from your google auth app 
                }
            }else if(data[0].responseCode === 600) {
                this.props.history.push('/authorize_device');
            }else {
                toastDisplay('error', data[0].responseMessage);
            }
        });
    }

    handleInputText(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        if (name === "auth_code") {
            this.setState({ [name]: value, api_Err_Msg: '' });
        }
    }

    // Verify code after user enters code
    verifyCode(e){
        e.preventDefault();
        if (this.state.auth_code == '') {
            this.setState({ api_Err_Msg: "Can't be blank" });
        }else{
            let disable_auth_request = {
                "userId": this.state.userDetail.userId,
                "key": this.state.two_factor_auth_type,
                "code": this.state.auth_code,
                "type":  this.state.display_sms_label ? 'sms' : 'code'
            }
            if(this.state.two_factor_auth_type == 22){
                disable_auth_request.clientTime = new Date().getTime();
            }
            return NodeAPI(disable_auth_request, 'disableAuth', 'POST')
            .then(responseJson => {
                if (responseJson.responseCode === 200) {
                    this.props.history.push('/security');
                } else if(responseJson.responseCode === 201) {
                    this.setState({ api_Err_Msg: responseJson.responseMessage });
                } else if(responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
        }
    }

    render(){
        return(
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>Disable Two-Factor Authentication</h1>
                    </div>
                </div>
                <div className="narrow id-verify-container">
                    <div className="section-wrap">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6 form-custom signup-form">
                                <div className="no-border">
                                    <form>
                                        {!this.state.display_sms_label ? 
                                            <p>{this.state.heading_msg}</p> 
                                            : 
                                            <p>SMS Verification Code</p> 
                                        }
                                        <div className="col-md-12">
                                            <label>Verification code</label>
                                            <input type="text" placeholder="Verification code" name="auth_code" value={this.state.auth_code} onChange={(event) => this.handleInputText(event)} />
                                            {this.state.api_Err_Msg != '' ? <p className="input-error-text">{this.state.api_Err_Msg}</p> : ''}
                                        </div>
                                        <div className="two-buttons-group btn-dist">
                                            <input type="submit" className="button-big lonely-button create-ad-mobile-margin" value="Disable" onClick={this.verifyCode} />
                                            <input type="submit" className="button-big lonely-button orange-bg" value="Back" onClick={() => {this.props.history.push('/security')}} /> 
                                        </div>
                                    </form>
                                    {(!this.state.display_sms_label && this.state.mobileVerifyStatus) ? 
                                    <a href="javascript:;" onClick={this.sendSMS}> Send me a SMS, I’ve lost my 2FA credentials. </a>
                                    : '' }
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default DisableAuthenticator;