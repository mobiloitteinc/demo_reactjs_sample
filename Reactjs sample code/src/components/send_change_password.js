// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

// Components
import Header from './header';
import Footer from './footer';

// Services
import { NodeAPI } from '../services/webServices'
import { user_detail, toastDisplay } from '../services/global';

class Equation extends Component {

    constructor(props){
        super(props);
        this.state = { // State initialization
            userDetail: user_detail,
            page_type: 'send_change_password',
            headerText: 'Change Password'
        }
        this.sendPasswordClk = this.sendPasswordClk.bind(this);
    }

    // Send password handler functionality
    sendPasswordClk(e){
        e.preventDefault();
        return NodeAPI({}, 'resetPassword?id='+this.state.userDetail.email ,'GET')
         .then(responseJson => {
            if(responseJson.responseCode == 200){
                this.setState({page_type: 'send_change_password_confirm', headerText: '<img src='+require("../assets/images/check.png")+'> Change Password'});
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    render(){
        return(
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>{ReactHtmlParser(this.state.headerText)}</h1>
                    </div>
                </div>
                <div className="narrow id-verify-container">
                    <div className="section-wrap">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6 form-custom signup-form">
                                {this.state.page_type == 'send_change_password' ? 
                                    <div className="no-border">
                                        <p> We will send you an email with instructions on how to change your password </p>
                                        <div className="two-buttons-group btn-dist">
                                            <input type="submit"  className="button-big lonely-button create-ad-mobile-margin" value="Reset password" onClick={this.sendPasswordClk} />
                                            <input type="submit"  className="button-big lonely-button orange-bg" value="Back" onClick={() => {this.props.history.push('/edit_profile')}} />
                                        </div>
                                    </div>  : 
                                    <div className="no-border">
                                        <p> We’ve sent you a password reset email to {this.state.userDetail.email}. Please, click the link in the email and set a new password for you account. </p>
                                    </div> 
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default Equation;