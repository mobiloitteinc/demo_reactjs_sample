// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';

// Components
import Header from './header';
import Footer from './footer';
import Loadingbar from './loader';

// Services
import { NodeAPI, thirdPartyAPI, machine_detail_api } from '../services/webServices';
import { user_token, user_detail, toastDisplay, Base64, setHomeFilter, setBuySellContent, numberWithCommas } from '../services/global';
import { coinMarketCapNano, binanceNano, kucoinNano } from "../services/APIData";

class TradesNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wholePageLoading: true,
            fiveRegxForDot: (/^(\d+)?([.]?\d{0,5})?$/),
            twoRegxForDot: (/^(\d+)?([.]?\d{0,2})?$/),
            userDetail: user_detail,
            trade_data: {},
            disableTradeReqBtn: user_detail == null ? true : false,
            trade_id: this.props.match.params.trade_id,
            country_name: '',
            local_in_nano: 0,
            binance_in_nano: 0,
            kucoin_in_nano: 0,
            updatedPrice: 0,
            localCurrency: '',
            priceStatus: '',
            nanoCurrency: '',
            similar_trade_list: [],
            similar_add_loader: false,
            ad_heading: ''
        }
        this.tradeReqBtn = this.tradeReqBtn.bind(this);
    }

    componentDidMount() {
        if(!$("footer.footer").hasClass("navbar-fixed-bottom")) $("footer.footer").addClass("navbar-fixed-bottom");
        this.getDeviceDetail()
        this.getYourAdvertisement();
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            wholePageLoading: true,
            trade_id: nextProps.match.params.trade_id,
            trade_data: {},
            localCurrency: '',
            priceStatus: '',
            nanoCurrency: ''
        }, () => {
            this.getYourAdvertisement();
        })
    }

    changeTrade(e, adDetail){
        e.preventDefault();
        this.props.history.push('/trades_new/'+Base64.encode(adDetail.advertisementId) );
    }

    // Get system current detail
    getDeviceDetail() {
        Promise.all([thirdPartyAPI({}, machine_detail_api, 'GET')]).then((data) => {
            let system_detail = data[0];
            this.setState({ country_name: system_detail.country_name });
        });
    }

    // get your advertsements detail
    getYourAdvertisement() {
        return NodeAPI({}, "getYourAdvertisement?id=" + this.state.trade_id, "GET")
            .then(responseJson => {
                if (responseJson.responseCode === 200) {
                    if(!responseJson.advertisementData.advertisementId) {
                        this.props.history.push('/');
                        return;
                    }
                    $("footer.footer").removeClass("navbar-fixed-bottom");
                    if(responseJson.advertisementData.tradeType == 'BUY'){
                        this.setState({ ad_heading: 'You are Selling Nano using '+responseJson.advertisementData.payment });
                    } else {
                        this.setState({ ad_heading: 'You are Buying Nano using '+responseJson.advertisementData.payment });
                    }
                    this.setState({ trade_data: responseJson }, () => {
                        this.getSimilarAdds();
                        this.setState({ wholePageLoading: false , updatedPrice: responseJson.advertisementData.totalPrice});
                    });
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                    this.setState({ wholePageLoading: false })
                }
                setTimeout(() => {
                    window.$('[data-toggle="tooltip"]').tooltip();
                },1000);
            })
    }

    // Handler for trade id field
    handleInputText(e, limitParam, limit) {
        e.preventDefault();
        if (e.target.value == '') {
            this.setState({ localCurrency: '', nanoCurrency: '' });
            return;
        }
        if (!this.state[limitParam].test(e.target.value)) {
            let tempVal = e.target.value.toString().split('.');
            e.target.value = tempVal[0] + '.' + tempVal[1].slice(0, limit);
        }
        const name = e.target.name;
        const value = parseFloat(e.target.value);
        if (name === "localCurrency") {
            this.setState({ [name]: value, nanoCurrency: (value / this.state.updatedPrice).toFixed(5) });
        } else if (name === "nanoCurrency") {
            this.setState({ [name]: value, localCurrency: (value * this.state.updatedPrice).toFixed(2) });
        }
    }

    handleField(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        if (name === "helloMsg") {
            this.setState({ [name]: value });
        }
    }
    
    // API to send the trade request
    tradeReqBtn(e) {
        e.preventDefault();
        this.setState({ disableTradeReqBtn: true });
        let trade_req = {
            "userId": this.state.userDetail.userId,
            "advertisementId": Base64.decode(this.state.trade_id),
            "tradePrice": this.state.localCurrency,
            "nanoPrice": this.state.nanoCurrency,
            "message": this.state.helloMsg,
            "tradeType": this.state.trade_data.advertisementData.tradeType == 'BUY' ? 'sell' : 'buy',
            "oldPrice": this.state.updatedPrice
        }
        return NodeAPI(trade_req, "trade", "POST")
            .then(responseJson => {
                this.setState({ disableTradeReqBtn: false });
                if (responseJson.responseCode == 200) {
                    this.props.history.push('/trade_request/'+Base64.encode(responseJson.tradeId));
                } else if (responseJson.responseCode == 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    this.setState({ priceStatus: responseJson.responseMessage })
                    // toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    switchToHome(type) {
        setBuySellContent(type.toUpperCase());
        if(type == 'buy'){
            setHomeFilter({paymentMethodId: this.state.trade_data.advertisementData.paymentMethodId});
        } else if(type == 'sell'){
            setHomeFilter({location: this.state.country_name});
        }
        this.props.history.push('/');
    }

    // get similar adds of user
    getSimilarAdds() {
        this.setState({ similar_add_loader: true })
        let similar_trade_req = {
            "userId": this.state.trade_data.user.userId,
            "tradeType": this.state.trade_data.advertisementData.tradeType.toLowerCase()  //buy or sell 
        }
        return NodeAPI(similar_trade_req, "similar-ads", "POST")
            .then(responseJson => {
                if (responseJson.responseCode === 200) {
                    let removeItem = responseJson.data.filter(x => Base64.encode(x.advertisementId) !== this.state.trade_id) // remove current advertisements from similar add list
                    this.setState({ similar_trade_list: removeItem, similar_add_loader: false })
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    getMinutes(start){
        let b = moment(start*1000);
        let a = moment(new Date().getTime());
        let minutes = a.diff(b, 'minutes');
        if(minutes <= 15) return({class: 'online-green', message: 'Online '+minutes+' minutes ago'});
        if(minutes > 15 && minutes <= 30) return({class: 'online-orange', message: 'Online '+minutes+' minutes ago'});
        if(minutes >30) return({class: 'online-gray', message: 'Online '+minutes+' minutes ago'});
    }

    getDifference(start){
        let b = moment(start*1000);
        let a = moment(new Date().getTime());
        let years = a.diff(b, 'year');
        b.add(years, 'years');
        let months = a.diff(b, 'months');
        b.add(months, 'months');
        let days = a.diff(b, 'days');
        b.add(days, 'days');
        let hours = a.diff(b, 'hours');
        b.add(hours, 'hours');
        let minutes = a.diff(b, 'minutes');

        if(years != 0){
            return('Online '+years + (years ==1 ? ' year' : ' years')+' ago');
        } else if(months != 0){
            return('Online '+months + (months ==1 ? ' month' : ' months')+ ' ago');
        } else if(days != 0){
            return('Online '+days + (days ==1 ? ' day' : ' days')+ ' ago');
        } else if(hours != 0){
            return('Online '+hours + (hours ==1 ? ' hour' : ' hours')+ ' ago');
        } else if(minutes != 0){
            return('Online '+minutes + (minutes ==1 ? ' minute' : ' minutes')+ ' ago');
        } else {
            return('Online')
        }
    }

    // Get payment according to the selection
    getPaymentDetail(item){
        if(item.cashVisibility){
            return ' : '+item.locationArea;
        } else if(item.labelVisibility){
            return ' : '+item.paymentMethodValue;
        } else return '';
    }

    listDropDown(item_id){
        $('#'+item_id).toggleClass("display-none");
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow">
                        <h1>{this.state.ad_heading}</h1>
                    </div>
                </div>
                {this.state.wholePageLoading ? <center className="loader-other"><BeatLoader /></center> :
                    <div className="narrow">
                        <div className="row trade-info-shift">
                            <div className="col-md-6">
                                <div className="row form-custom">
                                    <h2>How much would you like to {this.state.trade_data.advertisementData.tradeType == 'BUY' ? 'sell' : 'buy'}?</h2>
                                    <form>
                                        <div className="col-md-5">
                                            <label>Amount in Nano</label>
                                            <input type="number" className={`${this.state.priceStatus != '' ? 'input-error' : ''}`} placeholder="Nano" name="nanoCurrency" value={this.state.nanoCurrency} onChange={(evt) => this.handleInputText(evt, 'fiveRegxForDot', 5)} />
                                        </div>
                                        <div className="col-md-2" id="p-position">
                                            <p>=</p>
                                        </div>
                                        <div className="col-md-5">
                                            <label>Amount in {this.state.trade_data.advertisementData.currency.toUpperCase()}</label>
                                            <input type="number" className={`${this.state.priceStatus != '' ? 'input-error' : ''}`} placeholder={this.state.trade_data.advertisementData.currency.toUpperCase()} name="localCurrency" value={this.state.localCurrency} onChange={(evt) => this.handleInputText(evt, 'twoRegxForDot', 2)} />
                                        </div>
                                        <label className="w100">Message to trading partner</label>
                                        <textarea className={`${this.state.priceStatus != '' ? 'input-error' : ''}`} rows="10" cols="45" placeholder="Say your first message to your trading partner" name="helloMsg" value={this.state.helloMsg} onChange={(evt) => this.handleField(evt)}></textarea>

                                        {this.state.priceStatus != '' ?
                                            <p className="input-error-text">{this.state.priceStatus}</p> : ''
                                        }
                                        <br />
                                        <input className="button-big" type="submit" disabled={this.state.disableTradeReqBtn} onClick={this.tradeReqBtn} value="Request a trade" />

                                        {this.state.userDetail == null ?
                                            <p id="warning-sign-in"><Link to={'/sign_in'}>Sign In</Link> to start a trade!</p>  : ''
                                        }
                                    </form>
                                    <div className="pros-info pros-info-inner">
                                        <img className="icon" src={require('../assets/images/Shield.png')} />
                                        <h2>Security Information</h2>
                                        <p className="pros-text">
                                        Nanos will be hold in escrow after you start the trade. The buyer must make the payment and mark the trade as paid. The seller will verify the payment and release the Nanos.
                                        </p>
                                    </div>
                                </div>

                                {this.state.trade_data.advertisementData.tremOfTrade != '' ?
                                    <div className="row dropdown-info">
                                        <h2  onClick={() => {this.listDropDown('colblock3')}}>Terms of trade with {this.state.trade_data.user.userName}</h2>
                                        <img className=" collapse-arrow" onClick={() => {this.listDropDown('colblock3')}} src={require('../assets/images/arrow.png')} />
                                        <div className="col-md-12 right-info-text" id="colblock3">
                                            <p>{this.state.trade_data.advertisementData.tremOfTrade}</p>
                                        </div>
                                    </div> : null
                                }
                                
                                {this.state.trade_data.advertisementData.schedules.length != 0 ?
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="schedule-info-container">
                                                <div className="col-md-12"><h2>Working hours</h2></div>
                                                {this.state.trade_data.advertisementData.schedules.map((item, i) =>
                                                    <div key={i}>
                                                        {i != 0 ? <div className="working-hours-separator"></div> : '' }
                                                        <div className="row">
                                                            <div className="col-xs-3"><b>{item.day.substr(0, 3)}</b></div>
                                                            <div className="col-xs-4"><label>Open</label>
                                                                <p>{item.startTime}</p>
                                                            </div>
                                                            <div className="col-xs-5"><label>Close</label>
                                                                <p>{item.endTime}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div> : '' 
                                }
                            </div>
                            
                            <div className="col-md-6 info-right" id="trade-page-seller-info">
                                <div className="col-md-12"><h2 className="no-border">{this.state.trade_data.advertisementData.tradeType == 'BUY' ? 'Buyer' : 'Seller'} Information</h2></div>
                                <div className="row info-row">
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Price</span></div>
                                    <div className="col-md-5">
                                        <div className="info-row-value">
                                            {this.state.updatedPrice.toFixed(2)} {this.state.trade_data.advertisementData.currency} / {this.state.trade_data.advertisementData.cryptoCurrency.toUpperCase()}
                                        </div>
                                    </div>
                                </div>
                                <div className="row info-row">                        
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Payment Method</span></div>
                                    <div className="col-md-5">
                                        <div className="info-row-value">
                                            {this.state.trade_data.advertisementData.payment}{this.getPaymentDetail(this.state.trade_data.advertisementData)}
                                        </div>
                                    </div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">{this.state.trade_data.advertisementData.tradeType == 'BUY' ? 'Buyer' : 'Seller'} Username</span></div>
                                    <div className="col-md-5"><div className="info-row-value"><a href="javascript:;" onClick={() => this.props.history.push('/profile/' + this.state.trade_data.user.userName)}>{this.state.trade_data.user.userName}</a> 
                                    <a data-toggle="tooltip" data-placement="right" title={this.getDifference(this.state.trade_data.advertisementData.lastSeen)}>
                                        <div className={`${this.getMinutes(this.state.trade_data.advertisementData.lastSeen).class}`}></div>
                                    </a><p></p><span className="info-row-value">Rating {Math.round(this.state.trade_data.user.ratingPercentage)}% ({this.state.trade_data.user.ratingCount}) </span> <p> </p></div> </div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Trade Limits</span></div>
                                    <div className="col-md-5">
                                        <div className="info-row-value">
                                            {numberWithCommas(this.state.trade_data.advertisementData.minTransctionLimit)} - {numberWithCommas(this.state.trade_data.advertisementData.maxTransactionLimit)} {this.state.trade_data.advertisementData.currency.toUpperCase()}
                                        </div>
                                    </div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Location</span></div>
                                    <div className="col-md-5">
                                        <div className="info-row-value">{this.state.trade_data.advertisementData.worldWideStatus ? 'Worldwide' : this.state.trade_data.advertisementData.location}</div>
                                    </div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Payment Windows</span></div>
                                    <div className="col-md-5"><div className="info-row-value">{this.state.trade_data.advertisementData.paymentWindow} minutes</div></div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Id Verification Required</span></div>
                                    <div className="col-md-5"><div className="info-row-value">{this.state.trade_data.advertisementData.indentifyPeopleStatus ? 'Yes' : 'No'}</div></div>
                                </div>
                                <div className="row info-row"> 
                                    <div className="col-md-7 line-bg"><span className="info-row-bg">Phone Number Verification Required</span></div>
                                    <div className="col-md-5"><div className="info-row-value">{this.state.trade_data.advertisementData.smsVerifyStatus ? 'Yes' : 'No'}</div></div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="section-wrap">
                            <h2>Similar ads from {this.state.trade_data.user.userName}</h2>
                            <div className="ads-container">
                                <div className="row ads-heads">
                                    <div className="col-md-6"><a>Payment Method</a></div>
                                    <div className="col-md-2"><a>Price</a></div>
                                    <div className="col-md-3"><a>Limit</a></div>
                                    <div className="col-md-1"></div>
                                </div>
                                {this.state.similar_add_loader ?  
                                    <div className="row ads"><div className="col-md-12 empty-table-row"><Loadingbar /></div></div> :
                                    this.state.similar_trade_list.length != 0 ?
                                        this.state.similar_trade_list.map((item, i) =>
                                            <div className="row ads" key={i}>
                                                <div className="col-md-6">{item.paymentMethod}{this.getPaymentDetail(item)}</div>
                                                <div className="col-md-2 bold">{item.totalPrice.toFixed(2)} {item.currency}</div>
                                                <div className="col-md-3">{item.minTransactionLimit} - {item.maxTransactionLimit} {item.currency}</div>
                                                <div className="col-md-1"><a href="javascript:;" className={`table-button ${item.tradeType === 'BUY' ? 'red-bg' : ''}`} onClick={(evt) => {
                                                        this.changeTrade(evt, item)
                                                    }}>{item.tradeType === 'BUY' ? 'Sell' : 'Buy'}</a></div>
                                            </div>
                                        ) :
                                    <div className="row ads"><div className="col-md-12 empty-table-row">No similar ads...</div></div>
                                }
                            </div>
                            <p id="similar-ads">Browse similar ads from other users: 
                                <a href="javascript:;" onClick={() => { this.switchToHome('buy') }}> Buy Nano with {this.state.trade_data.advertisementData.payment}</a> or 
                                <a href="javascript:;" onClick={() => { this.switchToHome('sell') }}> Sell Nano in {this.state.country_name.toUpperCase()}</a>
                            </p>
                        </div> 
                    </div> 
                }
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default TradesNew;