// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import moment from 'moment';

// Component
import Loadingbar from './loader';

// Services
import { BuySellContent, user_token, toastDisplay, Base64, home_filter, setHomeFilter, numberWithCommas } from '../services/global';
import { NodeAPI } from '../services/webServices';
let filter_data = {currency_code:'', country_name:'', payment_method:'', worldWide:''};

class HomeBuy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buy_data: [],
            buyWorldLength: 0,
            loader: false,
            pageArr: [],
            current_index: 1,
            pageLimitIndex: 1
        }
    }

    // Event handler when changing on same component
    componentWillReceiveProps(nextProps, nextState){
        if(nextProps.filterData.currency_code !== filter_data.currency_code || nextProps.filterData.country_name !== filter_data.country_name || nextProps.filterData.payment_method !== filter_data.payment_method || nextProps.filterData.worldWide !== filter_data.worldWide || nextProps.filterData.sortFilter !== filter_data.sortFilter){
            filter_data.currency_code = nextProps.filterData.currency_code;
            filter_data.country_name = nextProps.filterData.country_name;
            filter_data.payment_method = nextProps.filterData.payment_method;
            filter_data.worldWide = nextProps.filterData.worldWide;
            filter_data.sortFilter = nextProps.filterData.sortFilter;
            this.setState({pageArr: [], current_index: 1, pageLimitIndex: 1,}, () => {
                this.getHomeBuyList(1, true);
            })
        } 
    }

    componentWillUnmount(){
        filter_data = {currency_code:'', country_name:'', payment_method:'', worldWide:''};
    }
    
    // get buy advertisement list
    getHomeBuyList(pageNumber, loaderDisplay) {
        if(loaderDisplay) this.setState({ loader: true });
        let buy_ad_list_req = {
            "cryptoCurrency": "nano",  
            "currency": filter_data.currency_code, 
            "location": filter_data.country_name, 
            "tradeType": "sell",       
            "paymentMethod": filter_data.payment_method,  
            "pageNo": pageNumber,
            "pageLimit": 10,
            "worldwide": filter_data.worldWide, 
            "sort": filter_data.sortFilter
        }
        return NodeAPI(buy_ad_list_req, 'advertisements', 'POST')
            .then(responseJson => {
                this.setState({ loader: false });
                if (responseJson.responseCode === 200) {
                    let length = responseJson.data.filter(x => x.isWorldWide);
                    this.setState({ buy_data: responseJson.data, buyWorldLength: length.length }, () => {
                        this.manageHistoryData(responseJson);
                    })
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
                setTimeout(() => {
                    window.$('[data-toggle="tooltip"]').tooltip();
                },1000);
            })
    }

    // Manage number of pagination on the basis of total feedbacks in db
    manageHistoryData(obj){
        let total = obj.totalResultCount;
        let page = (total)%10;
        let newTempArr = [];
        page == 0 ? page=Math.floor((total)/10) : page=Math.floor((total)/10)+1;
        for(let i=0;i<page;i++){
            newTempArr.push(i);
            if(i == page-1){
                this.setState({ pageArr: newTempArr });
            }
        }
    }

    // Change data on particular page click
    changeSearchPage = function(pageNumber){
		if(this.state.current_index != pageNumber){
            this.getHomeBuyList(pageNumber, true);
            this.setState({current_index: pageNumber});
		}
    }

    // Change page serials on next and previous
    changePageSerial = function(type){
		if(type == 'prev'){
            if(this.state.current_index != this.state.pageLimitIndex){
                this.getHomeBuyList(this.state.current_index - 1, true);
                this.setState({current_index: this.state.current_index - 1});
            }else{
                if(this.state.pageLimitIndex > 1){
                    this.getHomeBuyList(this.state.current_index - 1, true);
                    this.setState({pageLimitIndex: this.state.pageLimitIndex-5, current_index: this.state.current_index-1});
                } 
            }
		}else{
            if((this.state.current_index < this.state.pageLimitIndex+4) && (this.state.current_index <= this.state.pageArr.length-1)){
                this.getHomeBuyList(this.state.current_index + 1, true);
                this.setState({current_index: this.state.current_index + 1});
            }else{
                if(this.state.pageArr.length > (this.state.pageLimitIndex+5)) {
                    this.getHomeBuyList(this.state.current_index + 1, true);
                    this.setState({pageLimitIndex: this.state.pageLimitIndex+5, current_index: this.state.current_index+1});
                }
            }
		}
    }

    getMinutes(start) {
        let b = moment(start * 1000);
        let a = moment(new Date().getTime());
        let minutes = a.diff(b, 'minutes');
        if (minutes <= 15) return ({ class: 'online-green', message: 'Typically responds in minutes' });
        if (minutes > 15 && minutes <= 30) return ({ class: 'online-orange', message: 'User could take some time to answer' });
        if (minutes > 30) return ({ class: 'online-gray', message: 'User is offline, could take some time to answer' });
    }

    getPaymentDetail(item) {
        if (item.cashVisibility) {
            return ' : ' + item.locationArea;
        } else if (item.labelVisibility) {
            return ' : ' + item.paymentMethodValue;
        } else return '';
    }

    render() {
        return (
            <div className="narrow">
                <div className="section-wrap home-hover">
                    <div className="ads-container">
                        <div className="row ads-heads">
                            <div className="col-md-2"><a>Seller</a></div>
                            <div className="col-md-4"><a>Payment Method</a></div>
                            <div className="col-md-2"><a>Price</a></div>
                            <div className="col-md-3"><a>Limit</a></div>
                            <div className="col-md-1"></div>
                        </div>

                        {this.state.loader ? 
                            <div className="row ads"><div className="col-md-12 empty-table-row"><Loadingbar /></div></div> :

                            (this.state.buy_data.length - this.state.buyWorldLength) == 0 ?
                                <div className="row ads">
                                    <div className="col-md-12 empty-table-row">No offers have been found with the selected criteria. Please try changing the payment method or currency. </div>
                                </div> : 
                                <div>
                                    {this.state.buy_data.map((item, i) =>
                                        !item.isWorldWide ?
                                            <div className="row ads" key={i}>
                                                <div className="col-md-2">
                                                    <a href="javascript:;" onClick={() => this.props.history.push('/profile/' + item.userName)}>{item.userName}</a> 
                                                    <a data-toggle="tooltip" data-placement="right" title={this.getMinutes(item.lastSeen).message}>
                                                        <div className={`${this.getMinutes(item.lastSeen).class}`}></div>
                                                    </a>
                                                    <span className="rating-preview">({item.successfulTrade})</span>
                                                </div>
                                                <div className="col-md-4">{item.paymentMethod}{this.getPaymentDetail(item)}</div>
                                                <div className="col-md-2 bold">{item.totalPrice.toFixed(2)} {item.currency}</div>
                                                <div className="col-md-3">{numberWithCommas(item.minTransactionLimit)} - {numberWithCommas(item.maxTransactionLimit)} {item.currency}</div>
                                                <div className="col-md-1"><Link className="table-button" to={'/trades_new/' + Base64.encode(item.advertisementId)}>Buy</Link></div>
                                            </div> : ''
                                    )}
                                    {this.state.pageArr.length > 1 ? 
                                        <div className="row ads">
                                            <div className="pagination">
                                                <div className="pagination-section-container">
                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == 1) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('prev')}}>Previous</a>

                                                    {this.state.pageArr.slice(0, 5).map((item, i) =>
                                                        ((i+this.state.pageLimitIndex) <= this.state.pageArr.length) ? 
                                                        <a href="javascript:;" key={i} className={`pagination-before ${(this.state.current_index == i+this.state.pageLimitIndex) ? 'pagination-active' : ''}`} onClick={() => {this.changeSearchPage(i+this.state.pageLimitIndex)}}>{i+this.state.pageLimitIndex}</a>
                                                        : null
                                                    )}

                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == this.state.pageArr.length) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('next')}}>Next</a>
                                                </div>
                                            </div>
                                        </div> : ''
                                    }
                                </div>
                        }
                        
                    </div>
                </div>
                
                <div className="section-wrap">
                    <h2>Buy Nano Worldwide</h2>
                    <div className="ads-container">
                        <div className="row ads-heads">
                            <div className="col-md-2"><a>Seller</a></div>
                            <div className="col-md-4"><a>Payment Method</a></div>
                            <div className="col-md-2"><a>Price</a></div>
                            <div className="col-md-3"><a>Limit</a></div>
                            <div className="col-md-1"></div>
                        </div>
                        {this.state.loader ? 
                            <div className="row ads"><div className="col-md-12 empty-table-row"><Loadingbar /></div></div> :

                            this.state.buyWorldLength == 0 ?
                                <div className="row ads">
                                    <div className="col-md-12 empty-table-row">No offers have been found with the selected criteria. Please try changing the payment method or currency. </div>
                                </div> : 
                                <div>
                                    {this.state.buy_data.map((item, i) =>
                                        item.isWorldWide ?
                                            <div className="row ads" key={i}>
                                                <div className="col-md-2">
                                                    <a href="javascript:;" onClick={() => this.props.history.push('/profile/' + item.userName)}>{item.userName}</a> 
                                                    <a data-toggle="tooltip" data-placement="right" title={this.getMinutes(item.lastSeen).message}>
                                                        <div className={`${this.getMinutes(item.lastSeen).class}`}></div>
                                                    </a>
                                                    <span className="rating-preview">({item.successfulTrade})</span>
                                                </div>
                                                <div className="col-md-4">{item.paymentMethod}{this.getPaymentDetail(item)}</div>
                                                <div className="col-md-2 bold">{item.totalPrice.toFixed(2)} {item.currency}</div>
                                                <div className="col-md-3">{numberWithCommas(item.minTransactionLimit)} - {numberWithCommas(item.maxTransactionLimit)} {item.currency}</div>
                                                <div className="col-md-1"><Link className="table-button" to={'/trades_new/' + Base64.encode(item.advertisementId)}>Buy</Link></div>
                                            </div> : ''
                                    )}
                                    {this.state.pageArr.length > 1 ? 
                                        <div className="row ads">
                                            <div className="pagination">
                                                <div className="pagination-section-container">
                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == 1) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('prev')}}>Previous</a>

                                                    {this.state.pageArr.slice(0, 5).map((item, i) =>
                                                        ((i+this.state.pageLimitIndex) <= this.state.pageArr.length) ? 
                                                        <a href="javascript:;" key={i} className={`pagination-before ${(this.state.current_index == i+this.state.pageLimitIndex) ? 'pagination-active' : ''}`} onClick={() => {this.changeSearchPage(i+this.state.pageLimitIndex)}}>{i+this.state.pageLimitIndex}</a>
                                                        : null
                                                    )}

                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == this.state.pageArr.length) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('next')}}>Next</a>
                                                </div>
                                            </div>
                                        </div> : ''
                                    }
                                </div>
                        }
                    </div>
                </div>
                <div className="row" id="why-nano"><h2>Why use Nano?</h2>A fast and free way to pay for everything</div>
            </div>
        )
    }
}

export default HomeBuy;

