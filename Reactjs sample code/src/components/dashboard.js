// Modules
import React, { Component } from 'react';
import $ from 'jquery';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import { getTranslate, getActiveLanguage } from 'react-localize-redux';
import { connect } from 'react-redux';

// Components
import Header from './header';
import Footer from './footer';
import YourAd from './your_ad';
import YourTrade from './your_trade';

// services
import { user_token } from '../services/global'
import { NodeAPI, thirdPartyAPI } from '../services/webServices';
let global_path = '';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dashboard_tab_type: this.props.match.params.tab == 'trade' ? 'your_trades' : 'your_advertisment',
            filterOpen: this.props.match.params.type == 'opened' ? true : false
        }
        global_path = this.props.match.params.type;
    }

    // Receive props handler
    componentWillReceiveProps(nextProps){
        $(".dark-bg").fadeOut( 50 );
        if(global_path != nextProps.match.params.type){
            global_path = nextProps.match.params.type;
            if(global_path == 'opened') this.setState({filterOpen: true, dashboard_tab_type: nextProps.match.params.tab == 'trade' ? 'your_trades' : 'your_advertisment'});
            else this.setState({filterOpen: false, dashboard_tab_type: nextProps.match.params.tab == 'trade' ? 'your_trades' : 'your_advertisment'});
        }
    }

    // change tab functionality
    changeTab(type) {
        this.setState({dashboard_tab_type: type, filterOpen: false});
        if(type == 'your_trades') {
            this.props.history.push('/dashboard/trade/all');
        } 
        else {
            this.props.history.push('/dashboard/ad/all');
        }
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow">
                        <h1>Dashboard</h1>
                        <p className="tagline-h1">View and manage your advertisements and trades</p>
                        <div className="row">
                            <div className="col-md-12">
                                <ul className="nav nav-tabs">
                                        <li className={`${this.state.dashboard_tab_type == 'your_trades' ? 'active' : ''}`}><a data-toggle="tab" href="#your_trades" onClick={() => this.changeTab('your_trades')}>Your trades</a></li>
                                        <li className={`${this.state.dashboard_tab_type == 'your_advertisment' ? 'active' : ''}`}><a data-toggle="tab" href="#your_advertisment" onClick={() => this.changeTab('your_advertisment')}>Your advertisements</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="narrow">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="tab-content">
                                {this.state.dashboard_tab_type === 'your_trades' ?
                                    <YourTrade filterOpen={this.state.filterOpen} history={this.props.history} />
                                    :
                                    <YourAd filterOpen={this.state.filterOpen} history={this.props.history} />
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <Footer history={this.props.history} />
            </div>
        )
    }
}

// export default Dashboard;

const mapStateToProps = state => ({
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale).code
});

export default connect(mapStateToProps)(Dashboard);