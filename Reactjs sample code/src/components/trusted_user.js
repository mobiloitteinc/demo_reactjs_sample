// Module
import React, { Component } from 'react';
import $ from 'jquery';

//component
import Loadingbar from './loader';

// services
import { thirdPartyAPI, NodeAPI } from '../services/webServices';
import { user_token, user_detail, toastDisplay, Base64 } from '../services/global';

class TrustedUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetail: user_detail,
            trusted_user_list: [],
            marked_user_list: [],
            trusted_user_loader: false,
            marked_user_loader: false,
            trusted_filter: false,
            marked_filter: false,
            trusted_profile: '',
            marked_profile: '',
            trusted_profile_list: [],
            marked_profile_list: []
        }
    }

    componentDidMount() {
        setTimeout(() => {
            window.$('.selectpicker').selectpicker();
            window.$('#trusted_profile').selectpicker('val', '');
            window.$('#marked_profile').selectpicker('val', '');
        }, 1000)
        this.getTtrustedUsreList();
        this.getmarkedTrustedUser();
    }

    // get all trusted user list api
    getTtrustedUsreList() {
        this.setState({ trusted_user_loader: true })
        let trusted_user_req = {
            "userId": this.state.userDetail.userId
        }
        return NodeAPI(trusted_user_req, 'getMyTrustedUsers', 'POST')
            .then(responseJson => {
                this.setState({ trusted_user_loader: false})
                if (responseJson.responseCode === 200) {
                    this.setState({ trusted_user_list: responseJson.data, trusted_profile_list: responseJson.data })
                } else if (responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage)
                }
            })
    }

    // API to get the trusted users
    getmarkedTrustedUser() {
        this.setState({ marked_user_loader: true })
        let trusted_user_req = {
            "userId": this.state.userDetail.userId
        }
        return NodeAPI(trusted_user_req, 'trustedUsersWhoMarkedYou', 'POST')
        .then(responseJson => {
            this.setState({ marked_user_loader: false})
            if (responseJson.responseCode === 200) {
                this.setState({ marked_user_list: responseJson.data, marked_profile_list: responseJson.data })
            } else if (responseJson.responseCode === 600) {
                this.props.history.push('/authorize_device');
            } else {
                toastDisplay('error', responseJson.responseMessage)
            }
        })
    }

    open_close_filter(type) {
        if (type === 'form1') this.setState({ trusted_filter: !this.state.trusted_filter })
        else if (type === 'form2') this.setState({ marked_filter: !this.state.marked_filter })
    }

    handleInputText(e) {
        e.preventDefault();
        let id = e.target.id
        if (id === 'trusted_profile') {
            this.setState({ [id]: e.target.value })
        } else if (id === 'marked_profile') {
            this.setState({ [id]: e.target.value })
        }
    }

    filter(type, e) {
        e.preventDefault()
        if (type === 'trusted') {
            this.setState({ trusted_user_loader: true })
            if(this.state.trusted_profile === '') {
                this.getTtrustedUsreList();
            } else {
                let trusted_filter = this.state.trusted_user_list.filter(x => x.userName === this.state.trusted_profile)
                this.setState({ trusted_user_list: trusted_filter, trusted_user_loader: false })
            }
        } else if (type === 'marked') {
            this.setState({ marked_user_loader: true })
            if(this.state.marked_profile === '') {
                this.getmarkedTrustedUser();
            } else {
                let marked_filter = this.state.marked_user_list.filter(x => x.userName === this.state.marked_profile)
                this.setState({ marked_user_list: marked_filter, marked_user_loader: false })
            }
        }
    }

    // functioanlity of remove select item on click x icon
    removeSelect(select_box) {
        if (select_box === 'trusted_profile') {
            this.setState({ trusted_profile: '' })
            window.$('#trusted_profile').selectpicker('val', '');
        } else if (select_box === 'marked_profile') {
            this.setState({ marked_profile: '' })
            window.$('#marked_profile').selectpicker('val', '');
        } 
    }

    render() {
        return (
            <div id="trusted_user">
                <div className="section-wrap trusted-section">
                    <div className="row transaction-history">
                        <h2>Trusted users <span className="h2-filter-link" onClick={() => this.open_close_filter('form1')}>Filter<img src={require('../assets/images/arrow.png')} /></span></h2>
                        <form className={`${!this.state.trusted_filter ? 'display-none' : ''}`}>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Profile</label>
                                    <select data-live-search="true" title="Profile" className="form-control selectpicker" onChange={(evt) => this.handleInputText(evt)} id="trusted_profile">
                                        {this.state.trusted_profile_list.map((item, i) => <option value={item.userName} key={i} >{item.userName}</option>)}
                                    </select>
                                </div>
                                <div className="col-md-2">
                                    <input type="submit"  className="button-big filter-button mobile-no-margin" value="Apply" onClick={(e) => this.filter('trusted', e)} />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="ads-container  create-ad-mobile-margin">
                        <div className="row ads-heads">
                            <div className="col-md-6"><a>Profile</a></div>
                            <div className="col-md-6"><a>Has trades</a></div>
                        </div>
                        {this.state.trusted_user_loader ?
                            <div className="row ads"><div className="col-md-12 empty-table-row"><Loadingbar /></div></div> :
                            this.state.trusted_user_list.length !== 0 ?
                                this.state.trusted_user_list.map((item, i) =>
                                    <div className="row ads" key={i}>
                                        <div className="col-md-6" onClick={() => this.props.history.push('/profile/' + item.userName)}><a>{item.userName}</a> <a data-toggle="tooltip" data-placement="right" title="Typically takes more than 30 minutes to answer!"><div className="online-green"></div></a><span className="rating-preview">({item.count})</span></div>
                                        <div className="col-md-6"><b className={item.count !=0 ? "text-green" : "text-red"}>{item.count != 0 ? ' YES' : ' NO'}</b></div>
                                    </div>
                                ) :
                                <div className="row ads"><div className="col-md-12 empty-table-row">You have no trusted users yet</div></div>
                        }
                    </div>
                </div> 
                <div className="section-wrap">
                    <div className="row transaction-history">
                        <h2>Marked you as trusted <span className="h2-filter-link" onClick={() => this.open_close_filter('form2')}>Filter<img src={require('../assets/images/arrow.png')} /></span></h2>
                        <form className={`${!this.state.marked_filter ? 'display-none' : ''}`}>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Profile</label>
                                    <select data-live-search="true" title="Profile" className="form-control selectpicker" onChange={(evt) => this.handleInputText(evt)} id="marked_profile">
                                        {this.state.marked_profile_list.map((item, i) => <option value={item.userName} key={i} >{item.userName}</option>)}
                                    </select>
                                </div>
                                <div className="col-md-2">
                                    <input type="submit"  className="button-big filter-button mobile-no-margin" value="Apply" onClick={(e) => this.filter('marked', e)} />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="ads-container create-ad-mobile-margin">
                        <div className="row ads-heads">
                            <div className="col-md-6"><a>Profile</a></div>
                            <div className="col-md-6"><a>Has Trades</a></div>
                        </div>
                        {this.state.marked_user_loader ?
                            <div className="row ads"><div className="col-md-12 empty-table-row"><Loadingbar /></div></div> :
                            this.state.marked_user_list.length !== 0 ?
                                this.state.marked_user_list.map((item, i) =>
                                    <div className="row ads" key={i}>
                                        <div className="col-md-6" onClick={() => this.props.history.push('/profile/' + item.userName)}><a>{item.userName}</a> <a data-toggle="tooltip" data-placement="right" title="Typically takes more than 30 minutes to answer!"><div className="online-green"></div></a><span className="rating-preview">({item.count})</span></div>
                                        <div className="col-md-6"><b className={item.count !=0 ? "text-green" : "text-red"}>{item.count != 0 ? ' YES' : ' NO'}</b></div>
                                    </div>
                                ) :
                                <div className="row ads"><div className="col-md-12 empty-table-row">You have no trusted users yet</div></div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default TrustedUser;