// Modules
import React, { Component } from 'react';
import Recaptcha from 'react-recaptcha';
import { Link } from 'react-router-dom';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import moment from 'moment';
import 'moment-timezone';
import Loader from 'react-loader-spinner';
import * as $ from 'jquery';

// Components
import Header from './header';
import Footer from './footer';

// Services
import { validateUserName, validateEmail, validatePassword, submitValidatePassword } from "../services/validation_func";
import { NodeAPI, thirdPartyAPI, machine_detail_api } from '../services/webServices';
import { toastDisplay } from '../services/global';

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = { // State initialization
            page_type: 'sign_up',
            sign_up_token: null,
            captcha_verified: false,
            captchaErrMsg: '',
            recaptchaInstance: null,
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            usernameErrStatus: false,
            userNameErrMsg: '',
            emailErrStatus: false,
            emailErrMsg: '',
            passwordErrMsg: '',
            passwordErrStatus: false,
            cnfPassErrMsg: '',
            cnfPassErrStatus: false,
            time_zone: '',
            offset: '',
            signupBtnDisable: false,
            headerText: 'Sign Up'
        }
        this.registerClk = this.registerClk.bind(this);
        this.loadCallback = this.loadCallback.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.resendEmail = this.resendEmail.bind(this);
    }
    
    componentDidMount(){
        this.getDeviceDetail();
    }

    // Get system current detail
    getDeviceDetail(){
        Promise.all([thirdPartyAPI({}, machine_detail_api, 'GET')]).then((data) => {
            let system_detail = data[0];
            this.setState({time_zone: system_detail.time_zone.name, offset: moment().tz(system_detail.time_zone.name).format('Z')});
        });
    }

    // Captcha load callback
    loadCallback(response) {
        // console.log('loadCallback response => '+response);
    };

    // Captcha verify callback
    verifyCallback(response) {
        this.setState({ captcha_verified: true, captchaErrMsg: '' });
    };

    // Register button handler functionality
    registerClk(e) {
        e.preventDefault();
        this.setState({signupBtnDisable: true});
        if (validateUserName(this.state.username).status !== true) {
            this.setState({ usernameErrStatus: true, userNameErrMsg: validateUserName(this.state.username).error });
        }
        if (validateEmail(this.state.email).status !== true) {
            this.setState({ emailErrStatus: true, emailErrMsg: validateEmail(this.state.email).error });
        }

        if (submitValidatePassword(this.state.password).status !== true) {
            this.setState({ passwordErrStatus: true, passwordErrMsg: submitValidatePassword(this.state.password).error });
        }

        if (this.state.confirmPassword == '' || this.state.confirmPassword == undefined || this.state.confirmPassword == null) {
            this.setState({ cnfPassErrStatus: true, cnfPassErrMsg: "Can't be blank" })
        } else if (this.state.confirmPassword != this.state.password) {
            this.setState({ cnfPassErrStatus: true, cnfPassErrMsg: "Entered passwords are not equal" })
        }

        if (!this.state.captcha_verified) {
            this.setState({ captchaErrMsg: "The verification failed" })
        }
        setTimeout(() => {
            if(!this.state.usernameErrStatus && !this.state.emailErrStatus && !this.state.passwordErrStatus && !this.state.cnfPassErrStatus && this.state.captchaErrMsg == ""){
                this.signupApi()    // Signup API functionality 
            } else this.setState({signupBtnDisable: false});
        }, 500);
    }

    APIConfirm() {
        this.setState({captcha_verified: false});
        this.state.recaptchaInstance.reset(); 
    }

    // API to register the new user
    signupApi() {
        let signup_request_data = {
            "username": this.state.username,
            "email": this.state.email,
            "password": this.state.password,
            "timeZone":this.state.time_zone,
            "utcOffset":this.state.offset
        
        }
        return NodeAPI(signup_request_data, 'userSignup', 'POST')
            .then(responseJson => {
                this.setState({signupBtnDisable: false});
                if(responseJson.responseCode === 200) {
                    this.APIConfirm(); 
                    this.setState ({username: '', email: '', password: '', confirmPassword: '', page_type: 'sign_up_confirm', headerText: '<img src='+require("../assets/images/check.png")+'> Thank you for signing up!', sign_up_token: responseJson.token});
                    if(!$("footer.footer").hasClass("navbar-fixed-bottom")) $("footer.footer").addClass("navbar-fixed-bottom");
                }else if(responseJson.responseCode === 201) {
                    this.APIConfirm(); 
                    this.setState({ emailErrStatus: true, emailErrMsg: 'Has already been taken' })
                } else if(responseJson.responseCode === 202) {
                    this.APIConfirm(); 
                    this.setState({usernameErrStatus: true, userNameErrMsg: 'Has already been taken' })
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    // Resend email if not receive email on signup
    resendEmail(){
        return NodeAPI({}, 'resendEmail?token='+this.state.sign_up_token, 'GET')
        .then(responseJson => {
            if(responseJson.responseCode === 200) {
                this.setState({sign_up_token: null});
            }else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    // Handler for text fields
    handleInputText(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        if (name === 'username') {
            this.setState({ [name]: value.trim(), userNameErrMsg: '', usernameErrStatus: false });
        } else if (name === 'email') {
            this.setState({ [name]: value, emailErrMsg: '', emailErrStatus: false })
        } else if (name === 'password') {
            if (value === '') {
                this.setState({ [name]: value, passwordErrStatus: false, passwordErrMsg: '' });
            } else if (validatePassword(value).status !== true) {
                this.setState({ [name]: value, passwordErrStatus: true, passwordErrMsg: validatePassword(value).error });
            } else {
                this.setState({ [name]: value, passwordErrMsg: '', passwordErrStatus: false })
            }
        } else if (name === 'confirmPassword') {
            this.setState({ [name]: value, cnfPassErrStatus: false, cnfPassErrMsg: '' })
        }
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>{ReactHtmlParser(this.state.headerText)}</h1>
                    </div>
                </div>
                {this.state.page_type == 'sign_up' ?
                    <div className="narrow">
                        <div className="section-wrap">
                            <div className="row">
                                <div className="col-md-3"></div>
                                <div className="col-md-6 form-custom signup-form">
                                    <div className="no-border">
                                        <form>
                                            <div className="col-md-12">
                                                <label>Username</label>
                                                <input type="email" className={`${this.state.usernameErrStatus ? 'input-error' : ''}`} name="username" placeholder="Enter your username" onChange={(evt) => this.handleInputText(evt)} />
                                                {this.state.userNameErrMsg != '' ? <p className="input-error-text">{this.state.userNameErrMsg}</p> : ''}
                                            </div>
                                            
                                            <div className="col-md-12">
                                                <label>Email</label>
                                                <input type="email" className={`${this.state.emailErrStatus ? 'input-error' : ''}`} name="email" placeholder="Enter your email address" onChange={(evt) => this.handleInputText(evt)} />
                                                {this.state.emailErrMsg != '' ? <p className="input-error-text">{this.state.emailErrMsg}</p> : ''}
                                            </div>

                                            <div className="col-md-12">
                                                    <label>Password</label>
                                                    <input type="password" className={`${this.state.passwordErrMsg ? 'input-error' : ''}`} name="password" placeholder="Enter your password" onChange={(evt) => this.handleInputText(evt)} />
                                                    {this.state.passwordErrMsg != '' ? <p className="input-error-text">{this.state.passwordErrMsg}</p> : ''}
                                            </div>

                                            <div className="col-md-12">
                                                <label>Repeat password</label>
                                                <input type="password" className={`${this.state.cnfPassErrStatus ? 'input-error' : ''}`} name="confirmPassword" placeholder="Confirm your password" onChange={(evt) => this.handleInputText(evt)} onChange={(evt) => this.handleInputText(evt)} />
                                                {this.state.cnfPassErrMsg != '' ? <p className="input-error-text">{this.state.cnfPassErrMsg}</p> : ''}
                                            </div>
                                            <p className="text-align-center">Please, verify you are not a robot</p>
                                            <div className="gc-container">
                                                <Recaptcha ref={e => this.state.recaptchaInstance = e} sitekey="6LcOpU8UAAAAAHDLNvhFFuQZG0Sxyy4gb767xs_3" render="explicit" onloadCallback={this.loadCallback} verifyCallback={this.verifyCallback} />
                                                <p className="p-err">{this.state.captchaErrMsg}</p>
                                            </div>
                                        </form>
                                    </div>  
                                    <input type="submit"  className="button-big" value="Sign Up" disabled={this.state.signupBtnDisable} onClick={this.registerClk} />
                                    <p id="have-an-account">Already have an account? <Link to={'/sign_in'}>Sign In</Link></p>
                                </div>
                            </div>
                        </div>
                    </div> : 
                    <div className="narrow">
                        <div className="row section-wrap">
                            <div className="col-md-3"></div>
                            <div className="col-md-6">
                                <div className="simple-inner-container small-message-container">
                                    <p> Start selling and buying Nano! </p>
                                    <p> Please, check your inbox to verify your account. If you can’t find the confirmation email, you should check your spam folder.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default Register;