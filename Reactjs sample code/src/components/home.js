// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import { connect } from 'react-redux';
import { getTranslate, getActiveLanguage } from 'react-localize-redux';
import moment from 'moment';

// Component
import Header from './header';
import Footer from './footer';
import Loadingbar from './loader';
import BuyList from './home_buy';
import SellList from './home_sell'

// Services
import { BuySellContent, user_token, toastDisplay, Base64, home_filter, setHomeFilter, removeDuplicates } from '../services/global';
import { thirdPartyAPI, NodeAPI, machine_detail_api } from '../services/webServices';
import country_data from '../assets/js/countries.json';
import { countryDetail, paymentDetail } from "../services/APIData";

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            displayCookie: localStorage.getItem('cookie'),
            token: user_token,
            buy_sell_content: BuySellContent,
            country_list: countryDetail,
            payment_list: paymentDetail,
            ip: '',
            currency_code: 'USD',
            country_name: home_filter.location == undefined ? 'worldwide' : home_filter.location,
            payment_method: home_filter.paymentMethodId == undefined ? '' : home_filter.paymentMethodId,
            buy_data: [],
            sell_data: [],
            buyWorldLength: 0,
            sellWorldLength: 0,
            onlineMinutes: 0,
            sortFilter: 'rating',
            filterData: {
                "worldWide" : "",
                "currency_code": "",
                "country_name": "",
                "payment_method": "",
                "sortFilter": ""
            }
        }
    }

    componentDidMount() {
        this.loadJqueryEvents();
        window.$('.selectpicker').selectpicker();
        this.getDeviceDetail();
        let interval_handle = setInterval(() => { // setting countries detail
            if (countryDetail.length != 0 && paymentDetail.length != 0) {
                this.setState({ country_list: countryDetail, payment_list: paymentDetail });
                window.$('.selectpicker').selectpicker('refresh');
                if (home_filter.paymentMethodId == undefined) {
                    window.$('#payment_method').selectpicker('val', '');
                } else {
                    window.$('#payment_method').selectpicker('val', home_filter.paymentMethodId);
                }
                clearInterval(interval_handle);
            }
        }, 500);
    }

    // Load jquery events
    loadJqueryEvents(){
        $("#rating-toggle-link").click(function(e){
            $("#sorting-menu").fadeIn( 50 );
            e.stopPropagation();
        });

        $("body").click(function(e){
            $("#sorting-menu").fadeOut( 50 );
        });

        $("#sorting-menu").click(function(e){
            // e.stopPropagation();
        });
    }

    // Get system current detail
    getDeviceDetail() {
        Promise.all([thirdPartyAPI({}, machine_detail_api, 'GET')]).then((data) => {
            let system_detail = data[0];
            if (home_filter.location == undefined) {
                this.setState({ ip: system_detail.ip, currency_code: system_detail.currency.code, country_name: system_detail.country_name });
                window.$('#currency_code').selectpicker('val', system_detail.currency.code);
                window.$('#country_name').selectpicker('val', system_detail.country_name);
            } else {
                this.setState({ ip: system_detail.ip, currency_code: system_detail.currency.code });
                window.$('#currency_code').selectpicker('val', system_detail.currency.code);
            }
            setTimeout(() => {
                this.filterselect()
            }, 500);
        });
    }

    // when user select any filter call the function and send data to field component home_buy and home_sell 
    filterselect() {
        let { filterData } = this.state;
        filterData.worldWide = this.state.country_name == 'worldwide' ? true : false
        filterData.currency_code = this.state.currency_code
        filterData.country_name = this.state.country_name
        filterData.payment_method = this.state.payment_method
        filterData.sortFilter = this.state.sortFilter
        this.setState({filterData})
    }

    // On change of country, change their respective currency
    change_country(e) {
        let name = e.target.name
        if (name === 'country_name') {
            if (e.target.value === 'worldwide') {
                this.setState({ currency_code: 'USD', [name]: e.target.value}, () => {
                    this.filterselect();
                });
                window.$('#currency_code').selectpicker('val', 'USD');
            } else {
                let index = this.state.country_list.findIndex((x) => x.countryName === e.target.value);
                this.setState({ currency_code: this.state.country_list[index].countryCurrency, country_name: e.target.value}, () => {
                    this.filterselect();
                });
                window.$('#currency_code').selectpicker('val', this.state.country_list[index].countryCurrency);
            }
        } else if (name === 'currency_code') {
            this.setState({ [name]: e.target.value}, () => {
                this.filterselect();
            });
        } else if (name === 'payment_method') {
            this.setState({ [name]: e.target.value}, () => {
                this.filterselect();
            })
        } else if (name === 'buy_sell_content') {
            this.setState({ [name]: e.target.value}, () => {
                this.filterselect();
            })
        }
    }

    // Remove the payment selection
    removeSelect(selectType) {
        if (this.state[selectType] != '') {
            this.setState({ [selectType]: ''}, () => {
                window.$('#' + selectType).selectpicker('val', '');
                this.filterselect();
            })
        }
    }

    componentWillUnmount() {
        setHomeFilter({});
    }

    // Home page sorting change
    changeSortFilter(type){
        this.setState({sortFilter: type});
        setTimeout(() => { this.filterselect(); },500)
    }
    
    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header isHeader={true} history={this.props.history} />
                    <div className="narrow">
                        <div className="row">
                            <h1>Buy and Sell <img src={require('../assets/images/nano-logo.png')} /> in a Secure and Instantaneous Way</h1>
                            <p id="tag-line">Peer-to-Peer Nano Exchange</p>
                        </div>
                        <div className="row">
                            <div className="col-md-1"></div>
                            <div className="col-md-10">
                                <div className="ads-search-container">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <select className="form-control selectpicker" data-live-search="true" name="buy_sell_content" value={this.state.buy_sell_content} onChange={(event) => { this.change_country(event) }} id="buy_sell_content">
                                                <option value="BUY">Buy NANO <img src={require('../assets/images/arrow.png')} /></option>
                                                <option value="SELL">Sell NANO</option>
                                            </select>
                                        </div>
                                        <div className="col-md-3">
                                            <select data-live-search="true" className="form-control selectpicker" name="currency_code" title="Currency" value={this.state.currency_code} onChange={(event) => { this.change_country(event) }} id="currency_code">
                                                {/* <option value="USD" >USD</option> */}
                                                {removeDuplicates(this.state.country_list, 'countryCurrency').map((item, i) => <option value={item.countryCurrency} key={i}>{item.countryCurrency}</option>)}
                                            </select>
                                        </div>
                                        <div className="col-md-3">
                                            <select data-live-search="true" className="form-control selectpicker" name="country_name" title="Country" value={this.state.country_name} onChange={(event) => { this.change_country(event) }} id="country_name">
                                                <optgroup label="">
                                                    <option value="worldwide">Worldwide</option>
                                                </optgroup>
                                                {this.state.country_list.map((item, i) => <option value={item.countryName} key={i} >{item.countryName}</option>)}
                                            </select>
                                        </div>
                                        <div className="col-md-3">
                                            <select data-live-search="true" title="Type of offers" className="form-control selectpicker" name="payment_method" id="payment_method" onChange={(event) => { this.change_country(event, 'name') }}>
                                                {this.state.payment_list.map((item, i) => <option value={item.paymentMethodId} key={i} >{item.paymentOptions}</option>)}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 line-to-ads"><h2>Best {this.state.buy_sell_content == 'BUY' ? 'buy' : 'sell'} offers based on selected parameters</h2></div>
                            <div className="col-md-6 ads-sort">
                                <div>Sort by <a id="rating-toggle-link">{this.state.sortFilter}<img className="filter-arrow" src={require('../assets/images/arrow.png')} /></a>
                                    <ul className="" id="sorting-menu">
                                        <li onClick={() => {this.changeSortFilter('rating')}}><a href="javascript:;">Rating</a></li>
                                        <li onClick={() => {this.changeSortFilter('price')}}><a href="javascript:;">Price</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                {this.state.buy_sell_content == 'BUY' ?
                    <BuyList filterData={this.state.filterData} history={this.props.history} /> 
                : null}
                
                {this.state.buy_sell_content == 'SELL' ?
                    <SellList filterData={this.state.filterData} history={this.props.history}/> 
                : null}

                <div className="white-bg">
                    <div className="narrow">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="pros-info pros-container pros-container-left">
                                    <img className="icon icon-infinite" src={require('../assets/images/zero.png')} />
                                    <h2>Zero Fees</h2>
                                    <p className="pros-text">
                                        Pay for the purchase, not the privilege – zero fees on whatever you buy, from bus ticket to business class flight.
                                    </p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="pros-info pros-container pros-container-center">
                                    <img className="icon" src={require('../assets/images/transactions.png')} />
                                    <h2>Instant Transactions</h2>
                                    <p className="pros-text">
                                    Nano transactions happen immediately, so it's a currency you can use every day for purchases large or small.
                                    </p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="pros-info pros-container pros-container-right">
                                    <img className="icon icon-infinite" src={require('../assets/images/infinite.png')} />
                                    <h2>Infinitely Scalable</h2>
                                    <p className="pros-text">
                                    Nano can process over 1000x more transactions per second than Bitcoin, so you'll never get stuck in a queue. (source: nano.org)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <Footer history={this.props.history} />
                {this.state.displayCookie == null ? 
                    <div className="row cookies-warning">
                        <div className="col-md-12">
                            <h2>This Website Uses Cookies</h2>
                            <p>This website uses cookies to improve user experience. By using our website you consent to all cookies in accordance with our Privacy policy.</p>
                        </div>
                        <div className="col-md-12">
                            <input className="button-big lonely-button cookies-dismiss" type="submit" value="I Agree" onClick={() => {this.setState({displayCookie: false});localStorage.setItem('cookie',true)}} />
                        </div>
                    </div> : ''
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    translate: getTranslate(state.locale),
    currentLanguage: getActiveLanguage(state.locale)
});

export default connect(mapStateToProps)(Home);







