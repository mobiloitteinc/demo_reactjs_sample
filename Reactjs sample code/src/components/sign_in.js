// Modules
import React, { Component } from 'react';
import Recaptcha from 'react-recaptcha';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import Loader from 'react-loader-spinner';

// Components
import Header from './header';
import Footer from './footer';

// Services
import { login, set_user_detail, browser_fingerprint, system_info, toastDisplay, Base64 } from '../services/global';
import { NodeAPI, thirdPartyAPI, machine_detail_api } from '../services/webServices';
import { initSocket } from '../services/socket';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {  // State initialization
            captcha_verified: false,
            captchaErrMsg: '',
            recaptchaInstance: null,
            email: '',
            emailErrMsg: '',
            apiEmailErrMsg: '',
            emailErrStatus: '',
            password: '',
            passwordErrMsg: '',
            apiPassErrMsg: '',
            passwordErrStatus: '',
            rememberMe: false,
            apiErrMsg: '',
            apiErrStatus: false,
            ipAddress: '',
            btnDisabled: false,
            country_name: '',
            headerText: 'Sign In'

        }
        this.signinClk = this.signinClk.bind(this);
        this.loadCallback = this.loadCallback.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.sendAuthMail = this.sendAuthMail.bind(this);
    }

    componentDidMount() {
        $('.restricthack').bind("cut copy paste",function(e) {
            e.preventDefault();
        })
        if (localStorage.getItem('rememberMe') != null) {
            let rememberDetail = JSON.parse(localStorage.getItem('rememberMe'));
            if (rememberDetail.remember) {
                this.setState({ email: rememberDetail.email, rememberMe: rememberDetail.remember });
            }
        }
        this.getDeviceDetail();
    }

    // Get system current detail
    getDeviceDetail(){
        Promise.all([thirdPartyAPI({}, machine_detail_api, 'GET')]).then((data) => {
            let system_detail = data[0];
            this.setState({ipAddress: system_detail.ip, country_name: system_detail.country_name});
        });
    }

    // Captcha load callback
    loadCallback(response) {
        // console.log('loadCallback response => '+response);
    };

    // Captcha verify callback
    verifyCallback(response) {
        this.setState({ captcha_verified: true, captchaErrMsg: '' });
    };

    // Remember me functionality
    handleRemMe(e) {
        this.setState({ rememberMe: e.target.value == 'true' ? false : true });
    }

    // Signin button handler functionality
    signinClk(e) {
        e.preventDefault();
        this.setState({btnDisabled: true});
        if (this.state.email === '') {
            this.setState({ emailErrStatus: true, emailErrMsg: "Can't be blank", apiErrMsg: '', apiErrStatus: false });
        }
        if (this.state.password === '') {
            this.setState({ passwordErrStatus: true, passwordErrMsg: "Can't be blank", apiErrMsg: '', apiErrStatus: false });
        }
        if (!this.state.captcha_verified) {
            this.setState({ captchaErrMsg: "The verification failed" })
        }

        setTimeout(() => {
            if(!this.state.emailErrStatus && !this.state.passwordErrStatus && this.state.captchaErrMsg == "") {
                this.signInAPI()  // Signin API functionality
            } else {
                this.setState({btnDisabled: false});
            }
        }, 500);
    }

    APIConfirm(data) {
        if(data.responseCode === 200){
            localStorage.setItem('rememberMe',JSON.stringify({remember:this.state.rememberMe, email:this.state.email}));
            if(data.key == 11){
                this.setState({ captcha_verified: false, btnDisabled: false });
                this.state.recaptchaInstance.reset();
                this.props.history.push('/security_codes/'+data.userDetail.isPhoneVerified+'/print_code/'+Base64.encode(data.userDetail.userId));
            }else if(data.key == 22){
                this.setState({ captcha_verified: false, btnDisabled: false });
                this.state.recaptchaInstance.reset();
                this.props.history.push('/security_codes/'+data.userDetail.isPhoneVerified+'/mobile_code/'+Base64.encode(data.userDetail.userId));
            }else if(data.key == 44){
                this.saveDataOnLogin(data);
            }else if(data.key == 33){ // Login guard manage
                // this.setState({ captcha_verified: false, btnDisabled: false });
                if(data.hasOwnProperty('code')){
                    if(data.code == 600) this.saveDataOnLogin(data, 'authorize'); 
                }else {
                    this.saveDataOnLogin(data);
                }
            }
        }else if(data.responseCode === 201) {
            this.setState({ 
                captcha_verified: false, 
                apiEmailErrMsg: "Either your email address or password was incorrect.",  
                apiPassErrMsg: 'Either your email address or password was incorrect.', 
                apiEmailErrStatus: true,
                apiPassErrStatus: true,
                errorStatus: true,
                btnDisabled: false
            });
            this.state.recaptchaInstance.reset();
        } else {
            this.setState({ captcha_verified: false, btnDisabled: false });
            this.state.recaptchaInstance.reset();
            toastDisplay('error', data.responseMessage);
        }
    }

    // Afetr successful login, get user info from server and storing locally
    saveDataOnLogin(data, type){
        localStorage.setItem('token', data.userDetail.userId);
        let username = '';
        return NodeAPI({}, 'myProfile?id=' + Base64.encode(data.userDetail.userId)+'&userName='+username, 'GET')
        .then(responseJson => {
            this.setState({ captcha_verified: false, btnDisabled: false });
            this.state.recaptchaInstance.reset();
            if(responseJson.responseCode === 200) {
                set_user_detail(responseJson.userDetail);
                initSocket(responseJson.userDetail.userId);
                this.props.history.push('/');
            } else if(responseJson.responseCode === 600) {
                if(type == 'authorize') {
                    set_user_detail(responseJson.userDetail);
                    initSocket(responseJson.userDetail.userId);
                    this.sendAuthMail(responseJson.userDetail.userId);
                    this.props.history.push('/authorize_device');
                } 
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    // Login api to verify the credentials
    signInAPI() {
        let signin_request_data = {
            "ipAddrees": this.state.ipAddress,
            "email": this.state.email,        
            "password": this.state.password,
            "country": this.state.country_name,
        }
        return NodeAPI(signin_request_data, 'login', 'POST')
        .then(responseJson => {
            this.APIConfirm(responseJson);
        })
    }

    // Send mail to authorize the device
    sendAuthMail(userID){
        let auth_req = {
            "userId": userID,
            "browserKey": browser_fingerprint,
            "browserDetail": system_info
        }
        return NodeAPI(auth_req, 'sendEmailBrowserConfrim', 'POST')
            .then(responseJson => {
                if (responseJson.responseCode === 200) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    // Handler for text fields
    handleInputText(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        if (name === 'email') {
            this.setState({ [name]: value, emailErrMsg: '', emailErrStatus: false, apiEmailErrMsg: '', apiEmailErrStatus: false })
        } else if (name === 'password') {
            this.setState({ [name]: value, passwordErrMsg: '', passwordErrStatus: false, apiPassErrMsg: '', apiPassErrStatus: false })
        }
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>Sign In</h1>
                    </div>
                </div>
                <div className="narrow">
                    <div className="section-wrap">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6 form-custom signup-form">
                                <div className="no-border">
                                    <form>
                                        <div className="col-md-12">
                                            <label>Email</label>
                                            <input type="email" className={`${(this.state.emailErrStatus || this.state.apiEmailErrStatus) ? 'input-error' : ''}`} name="email" value={this.state.email} placeholder="Enter your email address" onChange={(evt) => this.handleInputText(evt)} />
                                            {this.state.emailErrMsg != '' ? <p className="input-error-text">{this.state.emailErrMsg}</p> : ''}
                                            {this.state.apiEmailErrMsg != '' ? <p className="input-error-text">{this.state.apiEmailErrMsg}</p> : ''}
                                        </div>
                                        
                                        <div className="col-md-12">
                                            <label>Password</label>
                                            <input type="password" className={`restricthack ${(this.state.passwordErrStatus || this.state.apiPassErrStatus) ? 'input-error' : ''}`} name="password" placeholder="Enter your account password" onChange={(evt) => this.handleInputText(evt)} />
                                            {this.state.passwordErrMsg != '' ? <p className="input-error-text">{this.state.passwordErrMsg}</p> : ''}
                                            {this.state.apiPassErrMsg != '' ? <p className="input-error-text">{this.state.apiPassErrMsg}</p> : ''}
                                        </div>

                                        <div className="col-md-6 custom-checkbox">
                                            <input type="checkbox" id="r1" name="rememberMe" value={this.state.rememberMe} checked={this.state.rememberMe} onChange={(event) => { this.handleRemMe(event); }} />
                                            <label htmlFor="r1"><span></span>Remember me</label>
                                        </div>
                                        <div className="col-md-6 forgot-password-section">
                                            <span><Link to={'/restore_password'}>Forgot password?</Link></span>
                                        </div>
                                        <p className="text-align-center">Please, verify you are not a robot</p>
                                        <div className="gc-container">
                                            <Recaptcha ref={e => this.state.recaptchaInstance = e} sitekey="6LcOpU8UAAAAAHDLNvhFFuQZG0Sxyy4gb767xs_3" render="explicit" onloadCallback={this.loadCallback} verifyCallback={this.verifyCallback} />
                                            {this.state.captchaErrMsg != '' ? <p className="input-error-text">{this.state.captchaErrMsg}</p> : ''}
                                        </div>
                                    </form>
                                </div>  
                                <input type="submit"  className="button-big" value="Sign In" disabled={this.state.btnDisabled} onClick={this.signinClk} />
                            </div>
                        </div>
                    </div>
                </div>
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default Login;