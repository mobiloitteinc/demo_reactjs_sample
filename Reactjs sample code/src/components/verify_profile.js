// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';

// Components
import Header from './header';
import Footer from './footer';

// Services
import { user_detail, set_edit_profile_tab, toastDisplay } from '../services/global'; 
import { NodeAPI } from '../services/webServices';

class VerifyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetail: user_detail,
            uuid: this.guid(),
            tradeId: '',
            tradeIdErr: '',
            bitcoin_username: '',
            bitcoin_usernameErr: '',
            localcoinBtnDisabled: false,
            headerText: 'Local Bitcoins Account Verification'
        }
        this.verifyProfile = this.verifyProfile.bind(this);
    }

    // Generate uuid
    guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + s4() + s4() + s4() + s4();
    }

    // Handler for trade id field
    handleInputText(e) {
        e.preventDefault() 
        const name = e.target.name;
        const value = e.target.value;
        if(name === "tradeId") {
            this.setState({[name]: value, tradeIdErr: ''});
        } 
        if(name === 'bitcoin_username'){
            this.setState({[name]: value, bitcoin_usernameErr: ''});
        }
    }

    verifyProfile(e) {
        e.preventDefault();
        this.setState({localcoinBtnDisabled: true});
        if(this.state.tradeId == '') {
            this.setState({tradeIdErr: "Can't be blank"})
        }
        if(this.state.bitcoin_username == '') {
            this.setState({bitcoin_usernameErr: "Can't be blank"})
        }
        setTimeout(() => {
            if(this.state.tradeIdErr == '' && this.state.bitcoin_usernameErr == ''){
                this.verifyProfileApi()    // Verify profile API functionality 
            } else this.setState({localcoinBtnDisabled: false});
        }, 500);
    }

    verifyProfileApi(){
        let verify_req = {
            "userId": this.state.userDetail.userId,
            "verifyUrl": this.state.tradeId,
            "localBitcoinUserName": this.state.bitcoin_username,
            "randomNumber": this.state.uuid
        }
        return NodeAPI(verify_req, 'verify-local-bitcoin', 'POST')
            .then(responseJson => {
                this.setState({localcoinBtnDisabled: false});
                if(responseJson.responseCode === 200) {
                    set_edit_profile_tab('verify_tab');
                    this.props.history.push('/edit_profile');
                } else if(responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow text-align-center">
                        <h1>Local Bitcoins Account Verification</h1>
                    </div>
                </div>
                <div className="narrow">
                    <div className="section-wrap">
                        <div className="row">
                            <div className="col-md-3"></div>
                            <div className="col-md-6 form-custom signup-form">
                                <div className="no-border">
                                    <form>
                                        <p>In order to verify your Local Bitcoins account and show it in your profile, you must follow this steps:</p>
                                        <p><b>1.</b> Create an advertisement in your Local Bitcoins account</p>
                                        <p><b>2.</b> Add this code in terms of trade field: <b>{this.state.uuid}</b></p>
                                        <p><b>3.</b> Enter in the field below Local Bitcoins advertisement URL you created: </p>
                                        <div className="col-md-12">
                                            <label>URL</label>
                                            <input type="text" placeholder="URL" name="tradeId" value={this.state.tradeId}  onChange={(evt) => this.handleInputText(evt)}/>
                                            {this.state.tradeIdErr != '' ? <p className="input-error-text">{this.state.tradeIdErr}</p> : ''}
                                        </div>
                                        <p><b>4.</b> Enter your Local Bitcoins username:</p>
                                        <div className="col-md-12">
                                            <label>Username</label>
                                            <input type="text" placeholder="Username" name="bitcoin_username" value={this.state.bitcoin_username}  onChange={(evt) => this.handleInputText(evt)}/>
                                            {this.state.bitcoin_usernameErr != '' ? <p className="input-error-text">{this.state.bitcoin_usernameErr}</p> : ''}
                                        </div>
                                        <p>Please, keep the advertisement active until LocalNano support team verify your profile. For avoid receiving trades, set an untradeable price.</p>
                                    </form>
                                    <div className="two-buttons-group btn-dist">
                                        <input type="submit"  className="button-big lonely-button create-ad-mobile-margin" value="Submit" disabled={this.state.localcoinBtnDisabled} onClick={this.verifyProfile} />
                                        <input type="submit"  className="button-big lonely-button orange-bg" value="Back" onClick={(evt) => {this.props.history.push('/edit_profile')}} />
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default VerifyProfile;