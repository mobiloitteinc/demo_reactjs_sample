// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner'; 
import $ from 'jquery';
import moment from 'moment';
import { BeatLoader } from 'react-spinners';

// Components
import Header from './header';
import Footer from './footer';
import UserProfile from './userProfile';
import BuyerSeller from './buyer_seller';
import Loadingbar from './loader';

// Services
import { user_detail, set_user_detail, edit_profile_tab, set_edit_profile_tab, set_security_tab, toastDisplay, Base64 } from '../services/global';
import { thirdPartyAPI, NodeAPI } from '../services/webServices';

class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            wholePageLoading: true,
            userDetail: null,
            userName: this.props.match.params.user_name,
            displayDetail: false,
            loggedInUser: false,
            userFound: false,
            trust: false,
            block: false,
            rating: 0,
            reputation: 0,
            feedback_list: [],
        }
    }

    componentDidMount(){
        if(!$("footer.footer").hasClass("navbar-fixed-bottom")) $("footer.footer").addClass("navbar-fixed-bottom");
        this.checkUser();
    }

    checkUser(){
        if(user_detail == null || user_detail.username != this.state.userName)this.setState({loggedInUser: false});
        else this.setState({userDetail: user_detail, loggedInUser: true});
        this.asyncCallApi('start'); 
    }

    componentWillReceiveProps(nextProps){
        // $('.user-dropdown').hide();
        $(".dark-bg").fadeOut( 50 );
        this.setState({userName: nextProps.match.params.user_name});
        setTimeout(() => {this.checkUser();},500)
    }

    // Get system current detail
    asyncCallApi(session){
        setTimeout(() => {
            let id='';
            NodeAPI({}, 'myProfile?id=' + id + '&userName=' + this.state.userName, 'GET')
            .then(responseJson => {
                if(responseJson.responseCode === 200) {
                    let reputation_percentage = 0;
                    let totalFeedback = responseJson.feedback.negativeFeedbackCount + responseJson.feedback.neutralFeedbackCount + responseJson.feedback.positiveFeedbackCount
                    if (responseJson.feedback.feedbackList.length !== 0) reputation_percentage = Math.round((responseJson.feedback.positiveFeedbackCount / totalFeedback) * 100);

                    this.setState({ userDetail: responseJson.userDetail, rating: totalFeedback, reputation: reputation_percentage, feedback_list: responseJson.feedback.feedbackList, userFound: true });

                    if(user_detail != null){
                        let trustIndex = responseJson.trustedPeoples.findIndex((x) => x.FROM_USER == user_detail.userId);
                        if(trustIndex != -1) this.setState({trust: true});
                        else this.setState({trust: false});
                        let blockIndex = responseJson.blockedPeoples.findIndex((x) => x.FROM_USER == user_detail.userId);
                        if(blockIndex != -1) this.setState({block: true});
                        else this.setState({block: false});

                        if(user_detail.username == responseJson.userDetail.username) set_user_detail(responseJson.userDetail);
                    }
                   
                    setTimeout(() => {
                        if(session == 'start'){
                            $("footer.footer").removeClass("navbar-fixed-bottom");
                            this.setState({ wholePageLoading: false });
                        }
                    }, 500);
                } else if(responseJson.responseCode === 201) {
                    localStorage.clear();
                    this.props.history.push('/sign_in');
                } else {
                    this.setState({ wholePageLoading: false });
                    toastDisplay('error', responseJson.responseMessage);
                }
                setTimeout(() => {
                    if(session == 'start') window.$('[data-toggle="tooltip"]').tooltip();
                },1000);
            });
        },500)
    }

    // TRUST, UNTRUST, BLOCK, UNBLOCK functionality
    actionUser(type) {
        if (user_detail == null) {
            this.props.history.push('/sign_in');
        } else {
            let trust_req = {};
            if (type == 'trust') {
                trust_req = {
                    fromUserId: user_detail.userId,
                    toUserId: this.state.userDetail.userId,
                    userStatus: this.state.trust ? 'UNTRUST' : 'TRUST'
                }
            } else {
                trust_req = {
                    fromUserId: user_detail.userId,
                    toUserId: this.state.userDetail.userId,
                    userStatus: this.state.block ? 'UNBLOCK' : 'BLOCK'
                }
            }
            NodeAPI(trust_req, 'block-trust-user', 'POST')
                .then(responseJson => {
                    if (responseJson.responseCode == 200) {
                        this.asyncCallApi('mid'); 
                    } else if (responseJson.responseCode != 201) {
                        toastDisplay('error', responseJson.responseMessage);
                    }
                })
        }
    }

    getMinutes(start){
        let b = moment(start*1000);
        let a = moment(new Date().getTime());
        let minutes = a.diff(b, 'minutes');
        if(minutes <= 15) return({class: 'online-green', message: 'Typically responds in minutes'});
        if(minutes > 15 && minutes <= 30) return({class: 'online-orange', message: 'User could take some time to answer'});
        if(minutes >30) return({class: 'online-gray', message: 'User is offline, could take some time to answer'});
    }

    // Getting last seen difference in terms of minutes, hours, days and year
    getDifference(start){
        let b = moment(start*1000);
        let a = moment(new Date().getTime());
        let years = a.diff(b, 'year');
        b.add(years, 'years');
        let months = a.diff(b, 'months');
        b.add(months, 'months');
        let days = a.diff(b, 'days');
        b.add(days, 'days');
        let hours = a.diff(b, 'hours');
        b.add(hours, 'hours');
        let minutes = a.diff(b, 'minutes');

        if(years != 0){
            return(years + (years ==1 ? ' year' : ' years')+' ago');
        } else if(months != 0){
            return(months + (months ==1 ? ' month' : ' months')+ ' ago');
        } else if(days != 0){
            return(days + (days ==1 ? ' day' : ' days')+ ' ago');
        } else if(hours != 0){
            return(hours + (hours ==1 ? ' hour' : ' hours')+ ' ago');
        } else if(minutes != 0){
            return(minutes + (minutes ==1 ? ' minute' : ' minutes')+ ' ago');
        } else {
            return('right now')
        }
    }

    // this use for check api data is null, undefined, zero, and blank or not.
    checkValidData(data, type) {
        if(data === null || data === undefined || data === '' || data === 0) return{value: '-'}
        else{
            if(type === 'minutes') return{value: data+' '+type}
            else if( type === 'firstPer') return {value: this.getDifference(data)}
            else if( type === 'account') return {value: this.getDifference(data)}
            else if( type === 'lastSeen') return {value: this.getDifference(data)}
        } 
    }

    render(){
        return(
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    {(!this.state.wholePageLoading && this.state.userFound) ? 
                        <div className="narrow">
                            <div className="row profile-overview">
                                <div className="col-md-4">
                                    <h1>{this.state.userName}
                                        <a data-toggle="tooltip" data-placement="right" title={this.getMinutes(this.state.userDetail.lastSeen).message}>
                                            <div className={`profile-name-online ${this.getMinutes(this.state.userDetail.lastSeen).class}`}></div>
                                        </a>
                                    </h1>
                                    <p className="profile-last-seen">Last seen {this.checkValidData(this.state.userDetail.lastSeen, 'lastSeen').value}</p>
                                    <p className="profile-reputation"><b>{this.state.reputation}%</b> - reputation based on <a>{this.state.rating} reviews</a></p>
                                    <p>Blocked by {this.state.userDetail.blockedPeopleCount} people<br />Trusted by {this.state.userDetail.trustedPeopleCount} people</p>
                                </div>
                                <div className="col-md-6" id="profile-intro">
                                    <p className="profile-info-text">{this.state.userDetail.introduction}</p>
                                    <div className="row profile-verifications">
                                        <div className="col-md-5">
                                            {this.state.userDetail.isEmailVerified ? 
                                                <span>
                                                    <img src={require('../assets/images/email-verified.png')} />
                                                    <p>E-mail verified</p>
                                                </span> : 
                                                <span>
                                                    <img src={require('../assets/images/email-not-verified.png')} /> 
                                                    <p>E-mail not verified</p>
                                                </span>
                                            }
                                        </div>
                                        <div className="col-md-6">
                                            {this.state.userDetail.isDocumentVerified ? 
                                                <span>
                                                    <img src={require('../assets/images/id-verified.png')} />
                                                    <p>ID verified</p>
                                                </span> : 
                                                <span>
                                                    <img src={require('../assets/images/id-not-verified.png')} /> 
                                                    <p>ID not verified</p>
                                                </span>
                                            }
                                        </div>
                                        <div className="col-md-5">
                                            {this.state.userDetail.isNanoVerified ? 
                                                <span>
                                                    <img src={require('../assets/images/local-verified.png')} />
                                                    <p>Local Bitcoins verified</p>
                                                </span> : 
                                                <span>
                                                    <img src={require('../assets/images/local-not-verified.png')} /> 
                                                    <p>Local Bitcoins not verified</p>
                                                </span>
                                            }
                                        </div>
                                        <div className="col-md-6">
                                            {this.state.userDetail.isPhoneVerified ? 
                                                <span>
                                                    <img src={require('../assets/images/phone-verified.png')} />
                                                    <p>Phone number verified</p>
                                                </span> : 
                                                <span>
                                                    <img src={require('../assets/images/phone-not-verified.png')} /> 
                                                    <p>Phone number not verified</p>
                                                </span>
                                            }
                                        </div>
                                    </div>
                                    <div className="row trust-untrust">
                                        {!this.state.loggedInUser ?
                                            <div className="col-md-12">
                                                <div className="button-big orange-bg " onClick={(e) => { this.actionUser('trust') }}>{this.state.trust ? 'Untrust' : 'Trust'}</div> 
                                                <span className="trust-untrust-or">or</span> 
                                                <div className="button-big red-bg" onClick={(e) => { this.actionUser('block') }}>{this.state.block ? 'Unblock' : 'Block'}</div>
                                            </div> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div> : '' 
                    }
                </div>
                
                {this.state.wholePageLoading ? <center className="loader-profile"><BeatLoader /></center> :
                    this.state.userFound ?  
                    <div className="narrow">
                        <div className="row profile-stats">
                            <div className="col-md-4">
                                <div className="row profile-stats-container" id="stats-left">
                                    <span>Successful Trades</span><p>{this.state.userDetail.successfullTrades}</p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="row profile-stats-container" id="stats-center">
                                    <span>Nano Trade Amount</span><p>{this.state.userDetail.tradeAmount}</p>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="row profile-stats-container" id="stats-right">
                                    <span>Different Partners</span><p>{this.state.userDetail.differentPartnerCount}</p>
                                </div>
                            </div>
                        </div>
                        
                        <div className="section-wrap">
                            <div className="row profile-info">
                                <div className="col-md-4">
                                    <div className="row profile-info-container">
                                        <label>Registered on</label><p>{this.checkValidData(this.state.userDetail.accountCreationDate, 'account').value}</p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="row profile-info-container">
                                        <label>First purchase</label><p>{this.checkValidData(this.state.userDetail.firstPurchase, 'firstPer').value}</p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="row profile-info-container no-border">
                                        <label>Average escrow release time</label><p>{this.checkValidData(this.state.userDetail.averageEscrowTime, 'minutes').value}</p>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    
                        <div className="section-wrap">
                            <h2>Feedback ({this.state.rating})<span className="h2-addition"><p>Feedback left by users that completed a trade with this user</p></span></h2>
                            <div className="ads-container">
                                {this.state.feedback_list.map((item, i) =>
                                    i < 3 ?
                                        <div key={i} className={`row ads ${(i == 0) ? 'no-border' : ''}`}>
                                            {item.ratingSignature === 'POSITIVE' ?
                                                <div>
                                                    <div className="col-md-6 feedback-text text-green">
                                                        <img src={require('../assets/images/positive-feedback.png')} />
                                                        <p>{item.feedback == '' ? 'Positive' : item.feedback}</p>
                                                    </div>
                                                    <div className="col-md-3">{item.nanoSignal} amount</div>
                                                    <div className="col-md-3 feedback-date">{moment(item.ratingTime * 1000).format('MMMM DD, YYYY, hh:mm A')}</div>
                                                </div>
                                                : item.ratingSignature === 'NEUTRAL' ?
                                                    <div>
                                                        <div className="col-md-6 feedback-text text-orange">
                                                            <img src={require('../assets/images/neutral-feedback.png')} />
                                                            <p>{item.feedback == '' ? 'Neutral' : item.feedback}</p>
                                                        </div>
                                                        <div className="col-md-3">{item.nanoSignal} amount</div>
                                                        <div className="col-md-3 feedback-date">{moment(item.ratingTime * 1000).format('MMMM DD, YYYY, hh:mm A')}</div>
                                                    </div> :
                                                    <div>
                                                        <div className="col-md-6 feedback-text text-red">
                                                            <img src={require('../assets/images/negative-feedback.png')} />
                                                            <p>{item.feedback == '' ? 'Negative' : item.feedback}</p>
                                                        </div>
                                                        <div className="col-md-3">{item.nanoSignal} amount</div>
                                                        <div className="col-md-3 feedback-date">{moment(item.ratingTime * 1000).format('MMMM DD, YYYY, hh:mm A')}</div>
                                                    </div>
                                            }
                                        </div>
                                        : ''
                                )}
                                {this.state.rating > 3 ?
                                    <div className="row browse-all-ads">
                                        <div className="col-md-12" onClick={() => {this.props.history.push('/feedback/'+this.state.userDetail.username)}}><a>Browse All</a></div>
                                    </div> : ''
                                }
                            </div>
                        </div>
                    </div> : ''
                }
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default Profile;