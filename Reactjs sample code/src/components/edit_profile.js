// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment-timezone';
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';

// Component
import Header from './header';
import Footer from './footer';
import Loadingbar from './loader';

// Services
import { user_detail, edit_profile_tab, set_edit_profile_tab, set_security_tab, toastDisplay, Base64 } from '../services/global';
import { thirdPartyAPI, NodeAPI } from '../services/webServices';
import { countryDetail } from "../services/APIData";

class EditProfile extends Component {

    constructor(props){
        super(props);
        this.state = {
            tabLoading: false,
            userDetail: user_detail,
            timezone_list: countryDetail,
            edit_tab_type: edit_profile_tab,
            browser_timezone: user_detail.timeZone,
            two_factor_auth: false,
            login_guard: false,
            introduction: user_detail.introduction,
            introductionErrMsg: '',
            saveProfile: false,
            saveNotifications: false,
        }
        this.saveProfile = this.saveProfile.bind(this);
        this.saveNotifications = this.saveNotifications.bind(this);
    }

    componentWillReceiveProps(nextProps){
        // $('.user-dropdown').hide();
    }

    componentDidMount(){
        if(!$("footer.footer").hasClass("navbar-fixed-bottom")) $("footer.footer").addClass("navbar-fixed-bottom");
        window.$('.selectpicker').selectpicker();
        this.asyncCallApi();
    }

    // Get system current detail
    asyncCallApi(){
        this.setState({tabLoading: true});
        Promise.all([NodeAPI({}, 'authenticationStatus?id='+Base64.encode(this.state.userDetail.userId) ,'GET')]).then((data) => {
            this.setState({tabLoading: false});
            $("footer.footer").removeClass("navbar-fixed-bottom");
            window.$('.selectpicker').selectpicker('refresh');
            if(data[0].responseCode === 600) {
                this.props.history.push('/authorize_device');
            } else if(data[0].responseCode === 200) {
                let security_status = data[0];
                this.setState({
                    two_factor_auth: security_status.securityStatus.two_factor_auth,
                    login_guard: security_status.securityStatus.login_guard,
                    email_verified: security_status.verificationStatus.email,
                });
            } else {
                toastDisplay('error', data[0].responseMessage);
            }
        })
    }

    change_timezone(e){
        this.setState({browser_timezone: e.target.value});
    }

    edit_tab_click(tab_type){
        this.setState({edit_tab_type: tab_type});
        set_edit_profile_tab(tab_type);
    }

    // Save user basic information
    saveProfile(e) {
        e.preventDefault();
        if (this.state.introduction.length > 200) {
            this.setState({ introductionErrMsg: "Can't be longer than 200 symbols" })
        } else {
            let user_info_request = {
                "userId": this.state.userDetail.userId,
                "timeZone": this.state.browser_timezone,
                "utcOffset": this.state.browser_timezone == '' ? '' :  moment().tz(this.state.browser_timezone).format('Z'),
                "introduction": this.state.introduction
            }
            this.setState({infoBtnDisable: true});
            return NodeAPI(user_info_request, 'saveProfile', 'POST')
            .then(responseJson => {
                this.setState({infoBtnDisable: false});
                if(responseJson.responseCode === 200) {
                    this.setState({ saveProfile: true })
                    setTimeout(() => {
                        this.setState({ saveProfile: false })
                    }, 1000)
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
        }
    }
    
    // Save user notifications
    saveNotifications(e) {
        e.preventDefault();
        let notificaton_req = {
            "authenticationStatus": this.state.successAuth,
            "chatMessageStatus": this.state.newChatMessage,
            "tradeContactStatus": this.state.newTrade,
            "onlinePaymentStatus": this.state.newOnlinePayment,
            "escrowReleaseStatus": this.state.newEscrow,
            "userId":this.state.userDetail.userId
        }
        this.setState({notificationBtnDisable: true});
        return NodeAPI(notificaton_req, 'notificationStatus', 'POST')
            .then(responseJson => {
                this.setState({notificationBtnDisable: false});
                if(responseJson.responseCode === 200) {
                    this.setState({ saveNotifications: true });
                    setTimeout(() => {
                        this.setState({ saveNotifications: false });
                    }, 1000)
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    handleTextArea(e) {
        e.preventDefault();
        this.setState({introduction: e.target.value, introductionErrMsg: ''})
    }

    // Notifications handler functionality
    handleNotifications(e) {
        this.setState({ [e.target.name]: e.target.value == 'true' ? false : true });
    }

    render(){
        return(
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    <div className="narrow">
                        <h1>Edit Profile</h1>
                        <div className="row">
                            <div className="col-md-2"></div>
                            <div className="col-md-8">
                                <ul className="nav nav-tabs">
                                        <li className={`nav-link ${this.state.edit_tab_type == 'basic_tab' ? 'active' : ''}`} onClick={() => {this.edit_tab_click('basic_tab')}}><a data-toggle="tab" href="#basic_tab">Basic Information</a></li>
                                        <li className={`nav-link ${this.state.edit_tab_type == 'security_tab' ? 'active' : ''}`} onClick={() => {this.edit_tab_click('security_tab')}}><a data-toggle="tab" href="#security_tab">Security</a></li>
                                        <li className={`nav-link ${this.state.edit_tab_type == 'verify_tab' ? 'active' : ''}`} onClick={() => {this.edit_tab_click('verify_tab')}}><a data-toggle="tab" href="#verify_tab">Verification</a></li>
                                        <li className={`nav-link ${this.state.edit_tab_type == 'notify_tab' ? 'active' : ''}`} onClick={() => {this.edit_tab_click('notify_tab')}}><a data-toggle="tab" href="#notify_tab">Notification</a></li>
                                </ul>
                            </div>
                        </div>
                                    
                    </div>
                </div>
                {this.state.tabLoading ? <center className="loader-other"><BeatLoader /></center> :
                    <div className="narrow">
                        <div className="section-wrap editprofile-shift">
                            <div className="section-wrap">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="tab-content">
                                            <div id="basic_tab" className={`tab-pane fade ${this.state.edit_tab_type == 'basic_tab' ? 'in active' : ''}`}>
                                                <div className="row">
                                                    <div className="col-md-2"></div>
                                                    <div className="col-md-8 simple-page-container">
                                                        <form>
                                                            <div className="row">
                                                                <div className="col-md-6">
                                                                    <label>Username</label>
                                                                    <input className="left-input" type="text" value={this.state.userDetail.username} disabled />
                                                                </div>

                                                                <div className="col-md-6">
                                                                    <label>Email</label>
                                                                    <input type="text" value={this.state.userDetail.email} disabled />
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-md-6">
                                                                    <label className="w100">Password</label>
                                                                    <input className="edit-password-input" type="password" value="**********" disabled />
                                                                    <input type="submit"  value="Change" className="button-big lonely-button password-change-btn" onClick={() => {this.props.history.push('/send_change_password')}} />
                                                                </div>
                                                                
                                                                <div className="col-md-6 create-ad-select">
                                                                    <label className="  ">Time Zone</label>
                                                                    <select className="form-control selectpicker" data-live-search="true" name="timezone" value={this.state.browser_timezone} onChange={(event) => {this.change_timezone(event)}}>
                                                                        <option value="">Select Timezone</option>
                                                                        {this.state.timezone_list.map((item, i) => <option value={item.timezoneValue} key={i}>{item.timezoneOptions}</option>)}
                                                                    </select>
                                                                </div>
                                                                    
                                                                <div className="col-md-12 create-ad-mobile-margin">
                                                                    <label className="create-ad-mobile-margin">Introduction</label>
                                                                    <textarea rows="10" cols="45" value={this.state.introduction} name="introduction" placeholder="Text shown on your public profile" onChange={(evt) => this.handleTextArea(evt)}></textarea>
                                                                    {this.state.introductionErrMsg != '' ? <p className="input-error-text">{this.state.introductionErrMsg}</p> : ''}
                                                                </div>
                                                            </div>
                                                            <input type="submit"  className="button-big lonely-button" value="Save" disabled={this.state.infoBtnDisable} onClick={this.saveProfile} />
                                                            {this.state.saveProfile ? 
                                                                <span style={{color: '#75D244'}}>Saved</span>: ''    
                                                            }
                                                        </form>
                                                    </div>
                                                    <div className="col-md-2"></div>
                                                </div>    
                                            </div>
                                                
                                            <div id="security_tab" className={`tab-pane fade ${this.state.edit_tab_type == 'security_tab' ? 'in active' : ''}`}>
                                                <div className="row">
                                                    <div className="col-md-2"></div>
                                                    <div className="col-md-8 simple-page-container security-container">
                                                        <div className="row security-option-row">
                                                            <div className="col-md-8"><h2>Account Security</h2></div>
                                                            <div className="col-md-4">
                                                                {(this.state.two_factor_auth == false && this.state.login_guard == false) ?
                                                                    <span>Weak</span> : 
                                                                    (this.state.two_factor_auth == true && this.state.login_guard == true) ?
                                                                    <span className="text-green">Strong</span> : 
                                                                    <span className="text-yellow">Medium</span>
                                                                }
                                                            </div>
                                                            <div className="col-md-12">
                                                                <p>To improve the security of your LocalNano account, enable two-factor authentication and login guard</p>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row">
                                                            <div className="col-md-8"><h2>Two Factor Authentication</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.two_factor_auth ? 'text-green' : ''}`}>
                                                                    {this.state.two_factor_auth ? 'Enabled' : 'Disabled'}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-8"><h2>Login Guard</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.login_guard ? 'text-green' : ''}`}>
                                                                    {this.state.login_guard ? 'Enabled' : 'Disabled'}
                                                                </span>
                                                            </div>
                                                            <div className="col-md-12" onClick={() => {set_security_tab('history_tab')}}>
                                                                <p className="view-login-history"><Link to={'/security'}>View Login History</Link></p>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <input type="submit"  className="button-big lonely-button security-option-button" value="Security Options" onClick={() => {set_security_tab('authenticate_tab'); this.props.history.push('/security')}} />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2"></div>
                                                </div> 
                                            </div>
                                                
                                            <div id="verify_tab" className={`tab-pane fade ${this.state.edit_tab_type == 'verify_tab' ? 'in active' : ''}`}>
                                                <div className="row">
                                                    <div className="col-md-2"></div>
                                                    <div className="col-md-8 simple-page-container security-container">
                                                        <div className="row security-option-row">
                                                            <div className="col-md-8"><h2>Email</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.email_verified ? 'text-green' : ''}`}>
                                                                    {this.state.email_verified ? 'Verified' : 'Not verified'}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row">
                                                            <div className="col-md-8"><h2>Phone Number</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.phone_verified ? 'text-green' : ''}`}>
                                                                    {this.state.phone_verified ? 'Verified' : 'Not verified'}
                                                                </span>
                                                            </div>
                                                            <div className="col-md-12">
                                                                {this.state.phone_verified ? 
                                                                    <Link to={'/verify_phone'}>Change your phone number</Link> :
                                                                    <Link to={'/verify_phone'}>Verify your phone number</Link>
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-8"><h2>ID</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.id_verified ? 'text-green' : ''}`}>
                                                                    {this.state.id_verified ? 'Verified' : 'Not verified'}
                                                                </span>
                                                            </div>
                                                            <div className="col-md-12">
                                                                {this.state.isdocPending == null ? 
                                                                    <Link to={'/verify_id'}>Verify your ID</Link>  : 
                                                                    <p>
                                                                    {
                                                                        this.state.isdocPending === 'SUBMITTED' ? 
                                                                        'Submitted - Pending approval' : 
                                                                        this.state.isdocPending === 'APPROVED' ? '' :
                                                                        <div>{this.state.isdocPending} - <Link to={'/verify_id'}>Verify your ID again</Link></div>
                                                                    }</p>
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-8"><h2>Local Bitcoins Account</h2></div>
                                                            <div className="col-md-4">
                                                                <span className={`${this.state.localBitCoin_verified ? 'text-green' : ''}`}>
                                                                    {this.state.localBitCoin_verified ? 'Verified' : 'Not verified'}
                                                                </span>
                                                            </div>
                                                            <div className="col-md-12">
                                                                {this.state.islocalBitcoinPending == null ? 
                                                                    <Link to={'/verify_profile'}>Verify your profile</Link>  : 
                                                                    <p>
                                                                    {
                                                                        this.state.islocalBitcoinPending === 'SUBMITTED' ? 
                                                                        'Submitted - Pending approval' : 
                                                                        this.state.islocalBitcoinPending === 'APPROVED' ? '' :
                                                                        <div>{this.state.islocalBitcoinPending} - <Link to={'/verify_profile'}>Verify your profile again</Link></div>
                                                                    }</p>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2"></div>
                                                </div> 
                                            </div>
                                                
                                            <div id="notify_tab" className={`tab-pane fade ${this.state.edit_tab_type == 'notify_tab' ? 'in active' : ''}`}>
                                                <div className="row">
                                                    <div className="col-md-2"></div>
                                                    <div className="col-md-8 simple-page-container security-container">
                                                        <div className="row security-option-row">
                                                            <div className="col-md-12 custom-checkbox">
                                                                <input type="checkbox" id="r1" name="successAuth" value={this.state.successAuth} checked={this.state.successAuth} onChange={(event) => { this.handleNotifications(event); }} />
                                                                <label htmlFor="r1"><span></span>Receive email notification on successful authentication</label>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row">
                                                            <div className="col-md-12 custom-checkbox">
                                                                <input type="checkbox" id="r2" name="newChatMessage" value={this.state.newChatMessage} checked={this.state.newChatMessage} onChange={(event) => { this.handleNotifications(event); }} />
                                                                <label htmlFor="r2"><span></span>Receive email notification about new chat messages</label>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-12 custom-checkbox">
                                                                <input type="checkbox" id="r3" name="newTrade" value={this.state.newTrade} checked={this.state.newTrade} onChange={(event) => { this.handleNotifications(event); }} />
                                                                <label htmlFor="r3"><span></span>Receive email notification for new trades</label>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-12 custom-checkbox">
                                                                <input type="checkbox" id="r4" name="newOnlinePayment" value={this.state.newOnlinePayment} checked={this.state.newOnlinePayment} onChange={(event) => { this.handleNotifications(event); }} />
                                                                <label htmlFor="r4"><span></span>Receive email notification for trades marked as paid</label>
                                                            </div>
                                                        </div>
                                                        <div className="side-menu-separator"></div>
                                                        <div className="row security-option-row no-border">
                                                            <div className="col-md-12 custom-checkbox">
                                                                <input type="checkbox" id="r5" name="newEscrow" value={this.state.newEscrow} checked={this.state.newEscrow} onChange={(event) => { this.handleNotifications(event); }} />
                                                                <label htmlFor="r5"><span></span>Receive email notification for completed trades</label>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <input type="submit"  className="button-big lonely-button security-option-button" value="Save" disabled={this.state.notificationBtnDisable} onClick={this.saveNotifications} />
                                                            {this.state.saveNotifications ? 
                                                                <span style={{color: '#75D244'}}>Saving...</span>: null    
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2"></div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <Footer history={this.props.history} /> 
            </div>
        )
    }
}

export default EditProfile;