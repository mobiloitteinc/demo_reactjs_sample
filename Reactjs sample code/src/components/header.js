// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import $ from 'jquery';
import moment from 'moment';
import { connect } from 'react-redux';
import { setActiveLanguage, getLanguages} from 'react-localize-redux';
import Loader from 'react-loader-spinner';
import { ToastContainer } from 'react-toastify';
import { BeatLoader } from 'react-spinners';

// Services
import { BuySellContent, setBuySellContent, user_token, logout, set_edit_profile_tab, set_security_tab, user_detail, set_user_detail, loadingSpinner, showLoading, toastDisplay, Base64, decimalCalc } from '../services/global';
import { NodeAPI } from '../services/webServices';
import { store } from '../services/store';
import { disconnectSocket, jsWebSocket } from '../services/socket';
let handle_userAuthrization = null;
class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            headType: '',
            token: user_token,
            userDetail: null,
            selectedLang: { code: 'EN', name: 'English' },
            headerText: '',
            notificationList: [],
            notificationListCount: 0,
            notificationLoader: false,
        }
        this.logoutClk = this.logoutClk.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        showLoading(nextProps.loading);
        return true;
    }

    // Logout functionality handler
    logoutClk() {
        this.setState({ headType: null, token: null, userDetail: null });
        disconnectSocket();
        logout();
        this.props.history.push('/sign_in');
    }

    // Header button click handler 
    headContentClk(type) {
        this.setState({ headType: type });
        if(type == 'help'){
            this.props.history.push('/help');
        } else if(type == 'wallet'){
            this.props.history.push('/wallet');
        } else if(type == 'notifications'){
            this.props.history.push('/notifications/all');
        } else{
            this.props.history.push('/');
        }
    }

    // Socket listener when any event fired
    socketListeners(){
        let self = this;
        jsWebSocket.addEventListener('message', function (event) {
            let tempObj = JSON.parse(event.data);
            // console.log('tempObj => '+JSON.stringify(tempObj));
            let tempProfile = self.state.userDetail;
            tempProfile.badgeCount = tempObj.badgeCount; 
            tempProfile.nanoBalance = tempObj.nanoBalance;
            self.setState({userDetail: tempProfile});
            if(tempObj.socketMessageType == 'chatInfo' || tempObj.socketMessageType == 'tradeInfo' || tempObj.socketMessageType == 'EvidenceAsk' || tempObj.socketMessageType == 'typingStart' || tempObj.socketMessageType == 'typingStop' || tempObj.socketMessageType == 'messageSeen'){
                if(self.props.history.location.pathname.split('/')[1] == 'trade_request'){
                    if(self.props.msgEvent) self.props.msgEvent(event.data);
                }
            } else if(tempObj.socketMessageType == 'profileInfo'){
                self.getProfileDetail(tempObj.data.userDetail.userId);
            } else if(tempObj.socketMessageType == 'walletInfo'){
                if(self.props.history.location.pathname.split('/')[1] == 'wallet'){
                    if(self.props.msgEvent) self.props.msgEvent(event.data);
                }
            }
        });
    }

    // Get profile detail
    getProfileDetail(id){
        let username = '';
        return NodeAPI({}, 'myProfile?id=' + Base64.encode(id)+'&userName='+username, 'GET')
        .then(profileJson => {
            if(profileJson.responseCode === 200) {
                set_user_detail(profileJson.userDetail);
                if(this.props.history.location.pathname.split('/')[1] == 'authorize_device') this.props.history.push('/');
            } else if(profileJson.responseCode === 600) {
                set_user_detail(profileJson.userDetail);
                this.props.history.push('/authorize_device');
            } else if(profileJson.responseCode === 201) {
                localStorage.clear();
                this.props.history.push('/sign_in');
            } else {
                toastDisplay('error', profileJson.responseMessage);
            }
        })
    }

    // Check user authorised browser
    checkUserAuthorized(userObj){
        if(userObj != null){
            if(userObj.isLoginGuard){
                if(!userObj.isAuthorisedUser){
                    this.props.history.push('/authorize_device'); 
                } else if(this.props.history.location.pathname.split('/')[1] == 'authorize_device') this.props.history.push('/');  
            } else {
                if(this.props.history.location.pathname.split('/')[1] == 'authorize_device') this.props.history.push('/');  
            }
        }
    }

    componentWillUnmount(){
        this.checkUserAuthorized(user_detail);
    }

    // Load particle js for home page UI
    componentDidMount() {
        this.loadJqueryEvents();
        let handle_socket_listener =  setInterval(() => { 
            if(this.state.userDetail != null){
                if(jsWebSocket.readyState == 1){
                    this.socketListeners();
                    clearInterval(handle_socket_listener);
                }
            }
        },500);
        this.setState({userDetail: user_detail});
        let tempVal = this.props.history.location.pathname.split('/')[1];
        if(tempVal == '' || tempVal == 'help' || tempVal == 'wallet' || tempVal == 'notifications'){
            this.setState({headType: BuySellContent});
        }

        let handle_userDetail =  setInterval(() => {
            if(user_detail != null){
                this.setState({userDetail: user_detail}, () => {
                    this.loadJqueryEvents();
                    this.checkUserAuthorized(user_detail);
                }); 
                clearInterval(handle_userDetail);
            }
        },500);
    }

    // get all list notification
    notificationApi() {
        this.setState({notificationLoader: true});
        let notification_req = {
            "notificationTo": this.state.userDetail.userId,
            "notificationType": "ALL", 
            "pageNo": 1,
            "pageLimit": 10,
            "readNotification": false,
        }
        return Promise.all([NodeAPI(notification_req, "get-notification", "POST")])
            .then(responseJson => {
                this.setState({notificationLoader: false});
                if (responseJson[0].responseCode === 200) {
                    if(responseJson[0].data.length <= 6) this.setState({ notificationList: responseJson[0].data, notificationListCount: responseJson[0].data.length })
                    else this.setState({ notificationList: responseJson[0].data.slice(0, 6), notificationListCount: responseJson[0].data.length });
                } else {
                    toastDisplay('error', responseJson[0].responseMessage);
                }
            })
    }

    notiPopup(){
        if($('.notifications-custom-popup').css('display') == 'none') {
            this.notificationApi();
            $(".notifications-custom-popup").fadeIn( 50 );
        } else $(".notifications-custom-popup").fadeOut( 50 );
    }

    // Load jquery events
    loadJqueryEvents(){
        $(".side-close").click(function(e){
            $(".dark-bg").fadeOut( 50 );
            e.stopPropagation();
        });
        
        $("#menu-stripes").click(function(e){
            $(".dark-bg").fadeIn( 50 );
            e.stopPropagation();
        });

        $(".language-selection-toggle").click(function(e){
            $("#language-menu").fadeIn( 50 );
            e.stopPropagation();
        });
            
        $("#top-image").mousemove(function(e){
            if($(window).width() > 992) {
                var pageX = e.pageX - ($(window).width() / 2);
                var pageY = e.pageY - ($(window).height() / 2);
                var newvalueX = width * pageX * -1 - 18;
                var newvalueY = height * pageY * -1 - 12;
                $('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
            } else {
                $('#top-image').css("background-position", 0+"px     "+0+"px");
            }
        });

        $( window ).resize(function() {
            $('#top-image').css("background-position", 0+"px     "+0+"px");
            var movementStrength = 25;
            var height = movementStrength / $(window).height();
            var width = movementStrength / $(window).width();
        });
    }

    // Change language handler
    changeLang(langObj) {
        if (langObj.code != this.state.selectedLang.code) {
            store.dispatch(setActiveLanguage(langObj.code))
            this.setState({ selectedLang: langObj });
        }
    }

    navigatePage(page){
        this.props.history.push(page);
    }

    render() {
        return (
            <div>
                <ToastContainer autoClose={3000} />
                <div className="row top-menu-adaptive">
                    <div className="col-md-12">
                        <div className="logo-language">
                            <img className="logo-localnano pointer_css" onClick={() => { this.headContentClk(null); }} src={require('../assets/images/logo.png')} />
                            <span>
                                <div className="language-selection-toggle">
                                    <img id="globe-icon" src={require('../assets/images/language.png')} />
                                    <span className="language-selection">{this.state.selectedLang.code} </span>
                                    <img id="menu-arrow" src={require('../assets/images/arrow.png')} />
                                </div>
                                <ul className="dropdown-menu display-block" id="language-menu">
                                    <li onClick={() => { this.changeLang({ code: 'EN', name: 'English' }) }}><a href="javascript:;">English</a></li>
                                    {/* <li onClick={() => { this.changeLang({ code: 'ru', name: 'Русский' }) }}><a href="javascript:;">Русский</a></li> */}
                                </ul>
                            </span>
                        </div>

                        {this.state.userDetail == null ?
                            <div className="hello-balance-notifications">
                                <span className="sign-in" id="wallet">
                                    <span className="pointer_css" onClick={() => {this.navigatePage('/sign_in');}}>SIGN IN</span>
                                </span>
                                <div className="sign-up" id="menu-notifications">
                                    <span className="pointer_css" onClick={() => {this.navigatePage('/sign_up');}}>SIGN UP</span>
                                </div>
                            </div> : 
                            <div className="hello-balance-notifications">
                                <span>Hello, {this.state.userDetail.username}!</span>
                                <span className="pointer_css" id="wallet" onClick={() => { this.headContentClk('wallet'); }}>
                                    <img id="nano-logo" src={require('../assets/images/walletnano')} />
                                    <span>
                                        {this.state.userDetail.nanoBalance > 0 ? decimalCalc(this.state.userDetail.nanoBalance) : 'Wallet'}
                                    </span>
                                </span>
                                <div id="menu-notifications" onClick={() => {this.notiPopup()}}>
                                    {this.state.userDetail.badgeCount == 0 ? '' : <div className="notifications-circle">{this.state.userDetail.badgeCount}</div>}
                                </div>
                            </div>
                        }
                        
                        <div className="menu-stripes">
                            {this.state.userDetail != null ?
                                <img id="menu-stripes" src={require('../assets/images/menu-stripes.png')} /> : '' }
                        </div>
                        
                    </div>
                </div>
                {this.state.userDetail != null ?
                    <div className="dark-bg">
                        <div className="side-menu-container">
                            <div className="row"><img className="side-close" src={require('../assets/images/close.png')} /></div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/dashboard.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/dashboard/trade/all'}>Dashboard</Link></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/trades.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/dashboard/trade/opened'}>Open Trades</Link></div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/createads.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/ad_new/new'}>Create an Advertisment</Link></div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/profile.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/profile/'+this.state.userDetail.username}>My profile</Link></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/edit.png')} /></div>
                                <div className="col-xs-9 side-menu-item" onClick={() => { set_edit_profile_tab('basic_tab') }}><Link to={'/edit_profile'}>Edit Profile</Link></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/security.png')} /></div>
                                <div className="col-xs-9 side-menu-item" onClick={() => { set_security_tab('authenticate_tab') }}><Link to={'/security'}>Account Security</Link></div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/wallet.png')} /></div>
                                <div className="col-xs-9 side-menu-item">
                                    <Link to={'/wallet'}>Wallet{this.state.userDetail.nanoBalance > 0 ? (' : '+decimalCalc(this.state.userDetail.nanoBalance) +' Nano') : ''}
                                    </Link>
                                </div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/trusted.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/trusted_ads/user'}>Trusted</Link></div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/support.png')} /></div>
                                <div className="col-xs-9 side-menu-item"><Link to={'/help'}>Support</Link></div>
                            </div>
                            <div className="row">
                                    <div className="col-md-3"></div>
                                    <div className="col-md-8 side-menu-separator"></div>
                            </div>
                            <div className="row side-menu-row">
                                <div className="col-xs-3 side-menu-icon"><img src={require('../assets/images/sidemenu/logout.png')} /></div>
                                <div className="col-xs-9 side-menu-item" onClick={this.logoutClk}><a href="javascript:;">Logout</a></div>
                            </div>
                        </div>
                    </div> : '' 
                }
                <div className="notifications-custom-popup">
                    {this.state.notificationLoader ? <center className="loader-noti"><BeatLoader /></center> :
                        <div>
                            {this.state.notificationList.length > 0 ?
                                <label id="notifications-title">NOTIFICATIONS</label> : 
                                <label>No notifications found</label>
                            }
                            <img className="pointer_css" id="notification-settings-icon" src={require('../assets/images/settings.png')} onClick={() => { this.headContentClk('notifications'); }} />
                            {this.state.notificationList.map((item, i) =>
                                <div key={i}>
                                    {item.notificationType === 'ALL' ? 
                                    <div className="row notification-row-container">
                                        <div className="col-xs-2 notification-icon-container"><img className="new-img" src={require('../assets/images/lock.png')} /></div>
                                        <div className="col-xs-10 notification-text-container">
                                            <div className="notification-label">new</div>
                                            <p>{item.notificationBody}</p>
                                            <label>{moment(item.notificationCreationTime).format('MM/DD/YYYY hh:mm A')}</label>
                                        </div>
                                    </div>
                                    : item.notificationType === 'MESSAGE' ?
                                    <div className="row notification-row-container">
                                        <div className="col-xs-2 notification-icon-container">
                                            {item.unreadCount === 0 ? 
                                                <img className="new-img" style={{top: 14+'px'}} src={require('../assets/images/bubble.png')} /> : 
                                                <img className="new-img" src={require('../assets/images/bubble.png')} />
                                            }
                                        </div>
                                        <div className="col-xs-10 notification-text-container">
                                            {item.unreadCount !== 0 ? 
                                                <div className="notification-label">new</div> : ''
                                            }
                                            <p className="pointer_css" onClick={() => this.props.history.push('/trade_request/' + Base64.encode(item.tradeId))}>New chat message {item.unreadCount !== 0 ? ' ('+item.unreadCount+') ' : ' '}
                                                from <a href="javascript:;">{item.notificationFromName}</a></p>
                                            <label>{moment(item.notificationCreationTime).format('MM/DD/YYYY hh:mm A')}</label>
                                        </div>
                                    </div>
                                    : item.notificationType === 'TRADE' ?
                                        <div className="row notification-row-container">
                                            <div className="col-xs-2 notification-icon-container">
                                                <img src={
                                                    item.notificationIcon === 'PAID' ? require('../assets/images/nano-notification.png') : 
                                                    item.notificationIcon === 'COMPLETE' ? require('../assets/images/check.png') :
                                                    item.notificationIcon === 'NEW_TRADE' ? require('../assets/images/add.png') :
                                                    item.notificationIcon === 'CANCELLED' ? require('../assets/images/cancel.png') :
                                                    item.notificationIcon === 'DISABLED' ? require('../assets/images/lock.png') :
                                                    require('../assets/images/check.png') 
                                                } />
                                            </div>
                                            <div className="col-xs-10 notification-text-container">
                                                <p className="pointer_css" onClick={() => this.props.history.push('/trade_request/' + Base64.encode(item.tradeId))}>{item.notificationBody}</p>
                                                <label>{moment(item.notificationCreationTime).format('MM/DD/YYYY hh:mm A')}</label>
                                            </div>
                                        </div>
                                    : (item.notificationType === 'NEWS' || item.notificationType === 'WALLET') ? 
                                        <div className="row notification-row-container">
                                            <div className="col-xs-2 notification-icon-container">
                                                <img src={item.notificationType === 'NEWS' ? require('../assets/images/bubble.png') : require('../assets/images/check.png')} />
                                            </div>
                                            <div className="col-xs-10 notification-text-container">
                                                <p>{item.notificationBody}</p>
                                                <label>{moment(item.notificationCreationTime).format('MM/DD/YYYY hh:mm A')}</label>
                                            </div>
                                        </div>
                                    : ''    
                                    }
                                </div>
                            )}
                            {this.state.notificationList.length > 0 ?
                            <div className="row notifications-browse" onClick={() => { this.headContentClk('notifications'); }}><a href="javascript:;">Browse All</a></div> : '' }
                        </div>
                    }
                </div>
            </div>
        )
    }
}

// export default Header;
const mapStateToProps = state => ({ languages: getLanguages(state.locale) });
const mapDispatchToProps = { setActiveLanguage };
export default connect(mapStateToProps, mapDispatchToProps)(Header);