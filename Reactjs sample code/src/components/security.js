// Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';
import moment from 'moment';
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';

// Component
import Header from './header';
import Footer from './footer';
import Loadingbar from './loader';

// Services
import { security_tab, set_security_tab, browser_fingerprint, system_info, user_detail, toastDisplay, Base64 } from '../services/global';
import { NodeAPI } from '../services/webServices';

class Security extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wholePageLoading: true,
            userDetail: user_detail,
            security_tab_type: security_tab,
            login_history_data: {loginHistory: []},
            two_factor_auth: false,
            login_guard: false,
            login_guard_browsers: [],
            pageArr: [],
            current_index: 1,
            pageLimitIndex: 1,
            historyTotalData: '',
            guardBtnDisabled: false
        }
        this.enableAuthClk = this.enableAuthClk.bind(this);
        this.enableLoginGuardClk = this.enableLoginGuardClk.bind(this);
    }

    componentDidMount() {
        this.account_security_api() // Authentication security functionality
    }

    componentWillReceiveProps(nextProps){
        // $('.user-dropdown').hide();
    }

    // Delete particular browser from authorized browsers
    deleteBrowser(e, item){
        let delete_req = {
            "userId": this.state.userDetail.userId,
            "loginGaurdId": item.loginGaurdId
        } 
        return NodeAPI(delete_req, 'deleteLoginGaurdHistory', 'POST')
            .then(responseJson => {
                if(responseJson.responseCode === 200) {
                    this.getLoginGuardDevices(1, 'source');
                } else if(responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    // Get security status and login history list
    account_security_api() {
        Promise.all([NodeAPI({}, 'authenticationStatus?id=' + Base64.encode(this.state.userDetail.userId), 'GET')]).then((data) => {
            this.setState({wholePageLoading: false});
            if (data[0].responseCode === 200) {
                let security_status = data[0];
                this.setState({
                    two_factor_auth: security_status.securityStatus.two_factor_auth,
                    login_guard: security_status.securityStatus.login_guard
                });

                if(this.state.security_tab_type == 'history_tab') this.getHistoryData(1, 'source');
                else if(this.state.security_tab_type == 'authenticate_tab') {
                    if(security_status.securityStatus.login_guard) this.getLoginGuardDevices(1, 'source');
                }
            } else {
                toastDisplay('error', data[0].responseMessage);
            }
        }) .catch(() => { this.setState({wholePageLoading: false}); });
    }

    // Manage number of pagination on the basis of total data in db
    manageHistoryData(obj){
        let total = obj.totalResultCount;
        let page = (total)%15;
        let newTempArr = [];
        page == 0 ? page=Math.floor((total)/15) : page=Math.floor((total)/15)+1;
        for(let i=0;i<page;i++){
            newTempArr.push(i);
            if(i == page-1){
                this.setState({ pageArr: newTempArr });
            }
        }
    }

    // Change page serials on next and previous
    changePageSerial = function(type){
		if(type == 'prev'){
            if(this.state.current_index != this.state.pageLimitIndex){
                if(this.state.security_tab_type == 'authenticate_tab') this.getLoginGuardDevices(this.state.current_index - 1);
                else this.getHistoryData(this.state.current_index - 1);
                this.setState({current_index: this.state.current_index - 1});
            }else{
                if(this.state.pageLimitIndex > 1){
                    if(this.state.security_tab_type == 'authenticate_tab') this.getLoginGuardDevices(this.state.current_index - 1);
                    else this.getHistoryData(this.state.current_index - 1);
                    this.setState({pageLimitIndex: this.state.pageLimitIndex-5, current_index: this.state.current_index-1});
                } 
            }
		}else{
            if((this.state.current_index < this.state.pageLimitIndex+4) && (this.state.current_index <= this.state.pageArr.length-1)){
                if(this.state.security_tab_type == 'authenticate_tab') this.getLoginGuardDevices(this.state.current_index + 1);
                else this.getHistoryData(this.state.current_index + 1);
                this.setState({current_index: this.state.current_index + 1});
            }else{
                if(this.state.pageArr.length > (this.state.pageLimitIndex+5)) {
                    if(this.state.security_tab_type == 'authenticate_tab') this.getLoginGuardDevices(this.state.current_index + 1);
                    else this.getHistoryData(this.state.current_index + 1);
                    this.setState({pageLimitIndex: this.state.pageLimitIndex+5, current_index: this.state.current_index+1});
                }
            }
		}
    }
    
    // Change data on particular page click
    changeSearchPage = function(pageNumber){
		if(this.state.current_index != pageNumber){
            if(this.state.security_tab_type == 'authenticate_tab') this.getLoginGuardDevices(pageNumber);
            else this.getHistoryData(pageNumber);
            this.setState({current_index: pageNumber});
		}
    }

    // Tab click handler
    security_tab_click(tab_type) {
        this.setState({ security_tab_type: tab_type, current_index: 1, pageLimitIndex: 1, pageArr: [] });
        set_security_tab(tab_type);
        if(tab_type == 'history_tab') {
            this.getHistoryData(1, 'source');
        } else if(tab_type == 'authenticate_tab') {
            if(this.state.login_guard) this.getLoginGuardDevices(1, 'source');
        }
    }

    // Save data on particular tab click
    getHistoryData(pageNumber, source){
        let history_req = {
            "userId": this.state.userDetail.userId,
            "pageNo": pageNumber,
            "pageLimit": 15
        }
        return NodeAPI(history_req, 'loginHistory', 'POST')
        .then(responseJson => {
            if (responseJson.responseCode === 200) {
                this.setState({ login_history_data: responseJson });
                if(source == 'source') this.manageHistoryData(responseJson);
            } else if(responseJson.responseCode === 600) {
                this.props.history.push('/authorize_device');
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    // Get login guard devices and display them
    getLoginGuardDevices(pageNumber, source){
        let device_req = {
            "userId": this.state.userDetail.userId,
            "pageNo": pageNumber,
            "pageLimit": 15
        }        
        return NodeAPI(device_req, 'loginGaurdHistory', 'POST')
            .then(responseJson => {
                if(responseJson.responseCode === 200) {
                    if(source == 'source') this.manageHistoryData(responseJson);
                    this.setState({login_guard_browsers: responseJson.loginGaurdHistory});
                } else if(responseJson.responseCode === 600) {
                    this.props.history.push('/authorize_device');
                } else {
                    toastDisplay('error', responseJson.responseMessage);
                }
            })
    }

    // Googgle auth handler
    enableAuthClk(e) {
        e.preventDefault();
        this.state.two_factor_auth ? this.props.history.push('/disable_authenticator') : this.props.history.push('/two_factor_auth');
    }

    // Login guard handler
    enableLoginGuardClk(){
        this.setState({guardBtnDisabled: true});
        if(this.state.login_guard){
            let disable_guard_request = {
                "userId": this.state.userDetail.userId,
                "key": 33,
                "code": '',
                "type":  '',
                "clientTime": new Date().getTime()
            }
            return NodeAPI(disable_guard_request, 'disableAuth', 'POST')
                .then(responseJson => {
                    this.setState({guardBtnDisabled: false});
                    if(responseJson.responseCode === 200) {
                        this.setState({login_guard: false});
                    } else if(responseJson.responseCode === 600) {
                        this.props.history.push('/authorize_device');
                    } else {
                        toastDisplay('error', responseJson.responseMessage);
                    }
                })
        }else{
            let login_guard_req={
                "userId": this.state.userDetail.userId,
                "browserKey": browser_fingerprint,
                "browserDetail": system_info
            }
            return NodeAPI(login_guard_req, 'loginGaurd', 'POST')
                .then(responseJson => {
                    this.setState({guardBtnDisabled: false});
                    if(responseJson.responseCode === 200) {
                        this.setState({ login_guard: true, current_index: 1, pageLimitIndex: 1, pageArr: [] });
                        this.getLoginGuardDevices(1, 'source');
                    } else if(responseJson.responseCode === 600) {
                        this.props.history.push('/authorize_device');
                    } else {
                        toastDisplay('error', responseJson.responseMessage);
                    }
                })
        }
        
    }

    render() {
        return (
            <div>
                <div className="darkblue-bg" id="top-image"> 
                    <Header history={this.props.history} />
                    {!this.state.wholePageLoading ?
                        <div className="narrow">
                            <h1>Account Security</h1>
                            <p className="security-preview">Your account security level: 
                                {(this.state.two_factor_auth == false && this.state.login_guard == false) ?
                                    <b className="text-red"> Weak</b> :
                                    (this.state.two_factor_auth == true && this.state.login_guard == true) ?   
                                    <b className="text-green"> Strong</b> :
                                    <b className="text-orange"> Medium</b> 
                                }
                                <br />
                                <label>To improve the security of your LocalNano account, enable two-factor authentication and login guard</label>
                            </p>
                            <div className="row">
                                <div className="col-md-12">
                                    <ul className="nav nav-tabs">
                                        <li className={`${this.state.security_tab_type == 'authenticate_tab' ? 'active' : ''}`} onClick={() => { this.security_tab_click('authenticate_tab') }}><a data-toggle="tab" href="#authenticate_tab">Login Protection</a></li>
                                        <li className={`${this.state.security_tab_type == 'history_tab' ? 'active' : ''}`} onClick={() => { this.security_tab_click('history_tab') }}><a data-toggle="tab" href="#history_tab">Login History</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> : '' 
                    }
                </div>
                
                {this.state.wholePageLoading ? <center className="loader-profile"><BeatLoader /></center> :
                    <div className="narrow">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="tab-content">
                                    <div id="authenticate_tab" className={`tab-pane fade ${this.state.security_tab_type == 'authenticate_tab' ? 'in active' : ''}`}>
                                        <div className="section-wrap trusted-section">
                                            <div className="simple-page-container">
                                                <div className="row">
                                                    <div className="col-md-5 secutity-section">
                                                        <h2>Two-Factor Authentication</h2>
                                                        <label>Status</label>
                                                        <p> 
                                                            {this.state.two_factor_auth ?
                                                                <b className="text-green">Enabled</b> :
                                                                <b className="text-red">Disabled</b>
                                                            }
                                                        </p>
                                                        <p>2FA, is an extra layer of security that requires not only a password and username but also a code genereted by an app in your phone.</p>
                                                        {this.state.two_factor_auth ?
                                                            <input type="submit"  className="button-big filter-button" value="Disable" onClick={this.enableAuthClk} /> :
                                                            <input type="submit"  className="button-big filter-button" value="Enable" onClick={this.enableAuthClk} />
                                                        }

                                                    </div>
                                                    <div className="col-md-1"></div>
                                                    <div className="col-md-5 secutity-section">
                                                        <h2>Login Guard</h2>
                                                        <label>Status</label>
                                                        <p> 
                                                            {this.state.login_guard ?
                                                                <b className="text-green">Enabled</b> :
                                                                <b className="text-red">Disabled</b>
                                                            }
                                                        </p>
                                                        <p>Prevent logins from unauthorized browsers.</p>
                                                        <p>&nbsp;</p>
                                                        {this.state.login_guard ?
                                                            <input type="submit"  className="button-big filter-button" value="Disable" disabled={this.state.guardBtnDisabled} onClick={this.enableLoginGuardClk} /> :
                                                            <input type="submit"  className="button-big filter-button" value="Enable" disabled={this.state.guardBtnDisabled} onClick={this.enableLoginGuardClk} />
                                                        }
                                                    </div>
                                                    <div className="col-md-1"></div>
                                                </div>
                                            </div>
                                        </div> 
                                        {(this.state.login_guard_browsers.length != 0 && this.state.login_guard) ?
                                            <div className="section-wrap">
                                                <div className="row">
                                                    <h2>User Agents</h2>
                                                </div>
                                                <div className="ads-container">
                                                    <div className="row ads-heads">
                                                        <div className="col-md-2"><a>Date</a></div>
                                                        <div className="col-md-7"><a>User agent</a></div>
                                                        <div className="col-md-2"><a>Current Browser</a></div>
                                                        <div className="col-md-1 remove-agent-history"></div>
                                                    </div>
                                                    {this.state.login_guard_browsers.map((item, i) =>
                                                        <div className="row ads" key={i}>
                                                            <div className="col-md-2">{moment(item.creationDate * 1000).format('MMMM DD, YYYY, HH:mm')}</div>
                                                            <div className="col-md-7">{item.userAgent}</div>
                                                            <div className="col-md-2">
                                                                {browser_fingerprint == item.browserKey ? 
                                                                    <b className="text-green">Yes</b> : 
                                                                    <b className="text-red">No</b>
                                                                }
                                                            </div>
                                                            <div className="col-md-1 remove-agent-history" onClick={(event) => {this.deleteBrowser(event, item)}}>
                                                                <a>Remove</a><img src={require('../assets/images/close.png')} />
                                                            </div>
                                                        </div>
                                                    )}
                                                    {(this.state.pageArr.length > 1 && this.state.login_guard) ? 
                                                        <div className="row ads">
                                                            <div className="pagination">
                                                                <div className="pagination-section-container">
                                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == 1) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('prev')}}>Previous</a>

                                                                    {this.state.pageArr.slice(0, 5).map((item, i) =>
                                                                        ((i+this.state.pageLimitIndex) <= this.state.pageArr.length) ? 
                                                                        <a href="javascript:;" key={i} className={`pagination-before ${(this.state.current_index == i+this.state.pageLimitIndex) ? 'pagination-active' : ''}`} onClick={() => {this.changeSearchPage(i+this.state.pageLimitIndex)}}>{i+this.state.pageLimitIndex}</a>
                                                                        : null
                                                                    )}

                                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == this.state.pageArr.length) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('next')}}>Next</a>
                                                                </div>
                                                            </div>
                                                        </div> : ''
                                                    }
                                                </div>
                                            </div> : '' 
                                        }
                                    </div>
                                    <div id="history_tab" className={`tab-pane fade ${this.state.security_tab_type == 'history_tab' ? 'in active' : ''}`}>
                                        <div className="section-wrap">
                                            <div className="ads-container login-history-container">
                                                <div className="ads-container login-history-container">
                                                    <div className="row ads-heads">
                                                        <div className="col-md-5"><a>Date</a></div>
                                                        <div className="col-md-5"><a>Type</a></div>
                                                        <div className="col-md-2"><a>IP address</a></div>
                                                    </div>
                                                    {this.state.login_history_data.loginHistory.map((item, i) =>
                                                        <div className="row ads" key={i}>
                                                            <div className="col-md-5">{moment(item.date * 1000).format('MMMM DD, YYYY, HH:mm')}</div>
                                                            <div className="col-md-5">{item.type}</div>
                                                            <div className="col-md-2">{item.ipAddress}</div>
                                                        </div>
                                                    )}
                                                    {this.state.pageArr.length > 1 ? 
                                                        <div className="row ads">
                                                            <div className="pagination">
                                                                <div className="pagination-section-container">
                                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == 1) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('prev')}}>Previous</a>

                                                                    {this.state.pageArr.slice(0, 5).map((item, i) =>
                                                                        ((i+this.state.pageLimitIndex) <= this.state.pageArr.length) ? 
                                                                        <a href="javascript:;" key={i} className={`pagination-before ${(this.state.current_index == i+this.state.pageLimitIndex) ? 'pagination-active' : ''}`} onClick={() => {this.changeSearchPage(i+this.state.pageLimitIndex)}}>{i+this.state.pageLimitIndex}</a>
                                                                        : null
                                                                    )}

                                                                    <a href="javascript:;" className={`pagination-before ${(this.state.current_index == this.state.pageArr.length) ? 'pagination-active' : ''}`} onClick={() => {this.changePageSerial('next')}}>Next</a>
                                                                </div>
                                                            </div>
                                                        </div> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row section-wrap">
                            <h2>Other Security Measures</h2>
                            <div className="row">
                                <div className="col-md-6 security-measure">
                                    <p><b>1.</b> Don’t use the same password on other websites. </p>
                                </div>
                                <div className="col-md-6 security-measure">
                                    <p><b>2.</b> Don’t log in LocalNano in others computers or devices, like ones in public cafes as they may have keyloggers installed to steal your credentials.</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 security-measure">
                                    <p><b>3.</b> Don’t install third party software, pirated software or browser add-ons you cannot trust 100%.</p>
                                </div>
                                <div className="col-md-6 security-measure">
                                    <p><b>4.</b> Before log in your account, read the browser address bar and double-check that you are logging in LocalNano.com and not another domain. Make sure that the spelling is correct: LOCALNANO.COM. Bookmark LocalNano.com in your browser and always use the bookmark when using the platform. </p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 security-measure">
                                    <p><b>5.</b> Create an email for your LocalNano account that is not used for anything else. Don’t give this address to your trading parties, use a separate email.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <Footer history={this.props.history} />
            </div>
        )
    }
}

export default Security;