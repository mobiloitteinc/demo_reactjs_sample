// Modules
import React from 'react';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';

// import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory'
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import $ from 'jquery';
import Fingerprint from 'fingerprintjs';
import { Provider } from 'react-redux';
import { BeatLoader } from 'react-spinners';

// Components
import Home from './components/home';
import SignUp from './components/sign_up';
import SignIn from './components/sign_in';
import Help from './components/help';
import Guides from './components/guides';
import CreateAccount from './components/create_account';
import HowToBuy from './components/how_to_buy';
import HowToSell from './components/how_to_sell';

// Services
import { set_user_detail, set_browser_fingerprint, set_system_info, toastDisplay, Base64, changeTabTitle, checkSession } from './services/global';
import { NodeAPI } from './services/webServices';
import { getMarketPrices, getCountryDetail, getAdminConfig } from './services/APIData';
import { initializeOneSignalPush } from "./services/OneSignal";
import { store } from './services/store';
import { initSocket } from './services/socket';

const history = createHistory();
history.listen((location, action) => {
    changeTabTitle();
    window.scrollTo(0, 0)
});

// Managing route loggedin and loggedout user
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      localStorage.getItem('token') != null
        ? <Redirect to='/' />
        : <Component {...props} />
    )} />
)

class App extends React.Component {

  constructor(props){
    super(props);
  }

  componentDidMount(){
    checkSession(); // session checking
    set_browser_fingerprint(new Fingerprint().get()); // setting fingerprint id
    set_system_info(); // setting system info
    getAdminConfig(); // get admin configuration
    
    /*** Handle cases on app load ***/
    if(localStorage.getItem('token') != null){
        let username = '';
        NodeAPI({}, 'myProfile?id=' + Base64.encode(localStorage.getItem('token'))+'&userName='+username, 'GET')
        .then(responseJson => {
            if(responseJson.responseCode === 200) {
                set_user_detail(responseJson.userDetail);
                initSocket(responseJson.userDetail.userId);
                this.setState({ready_detail: true});
            } else if(responseJson.responseCode === 201) {
                localStorage.clear();
                history.push('/sign_in');
                this.setState({ready_detail: true});
            } else {
                toastDisplay('error', responseJson.responseMessage);
            }
        })
    }

    $( document ).bind( "mousedown", function(e) {
        if(e.button == 0){
            checkSession(); // session checking
        }
    });
  }

  render() {
    return (
        <Provider store={store}>
            <Router history={history}>
                <div>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <PrivateRoute path='/sign_up' component={SignUp} />
                        <PrivateRoute path='/sign_in' component={SignIn} />
                        <Route exact path='/help' component={Help} />
                        <Route exact path='/guides' component={Guides} />
                        <Route exact path='/create_account' component={CreateAccount} />
                        <Route exact path='/how_to_buy' component={HowToBuy} />
                        <Route exact path='/how_to_sell' component={HowToSell} />
                        <Route exact path='/how_to_post' component={HowToPost} />
                        <Route exact path='/security_tips' component={SecurityTips} />
                        <Route exact path='/equation' component={Equation} />
                        <Route exact path='/about' component={About} />
                        <Route exact path='/user_agreement' component={UserAgreement} />
                        <Route exact path='/privacy_policy' component={PrivacyPolicy} />
                        <Route exact path='/trades_new/:trade_id' component={TradesNew} />
                        <Route exact path='/ad_new/:id' component={AdNew} />
                        <Route exact path='/profile/:user_name' component={Profile} />
                    </Switch>
                </div>
            </Router>
        </Provider>
    );
  }
}

export default App;
